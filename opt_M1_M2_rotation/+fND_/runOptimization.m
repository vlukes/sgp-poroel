%% optimization
i0 = 0;
ppen = -1;
reg = 0;
rad = 0;

%% continuation with SIMP-type scheme
for i = 1:length(iterations)
    if i > 1
        i0 = sum(iterations(1:i-1));
    end
    imax = i0 + iterations(i);
    % main sgp algorithm
    [xopt{i+1},ppen] = fND_.sgp_multimat_M1_M2(Geo,Material1,Material2,cellType,Algparam,imax,reg,rad,xopt{i},i0,ppen,lambda_phi,lambda_psi,sfepyFile);
    
end

matfile = Material1.matfile;
save(strcat(Geo.problem,'.mat'), 'xopt', 'Geo', 'matfile')
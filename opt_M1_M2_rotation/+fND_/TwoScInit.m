function [Material] = TwoScInit(Material,Algparam,matFile)

Material.x_fine = Material.a; 
nsamples_fine = size(Material.a,2);

% check which material structure we are setting by checking how many design variables we have
% 2 design variables: unit cell with three fluid channels
% 1 design variable: unit cell with void inclusion
assert(isfile(matFile));
% load(matFile.x2, 'x2');
load(matFile);

inclusion = 0;

if isempty(who('-file', matFile, 'x2'))
  inclusion = 1;
end

if inclusion
  tmp = eye(3,3)*1e-3;
  Kglob = permute(repmat(tmp,1,1,nsamples_fine), [3 1 2]);
  
  invtmp = eye(3,3)*1e3;
  Kglobinv = permute(repmat(invtmp,1,1,nsamples_fine), [3 1 2]);
else
  X3 = zeros(nsamples_fine,1);
  PPenf = zeros(nsamples_fine,1);
  for k=1:nsamples_fine
    %% Pglob(i,j,k) = f3D_.truevolume(x(i),x(j),x(k)); % for analytic resourse function
    PPenf(k) = 0; %f3D_.penalize0(x(i),x(j),x(k),Algparam.thres);
    %             X1(i,j,k) = x(i);
    %             X2(i,j,k) = x(j);
    X3(k) = angle(k);
  end
end

Material.Aglob = Aglob;
Material.Kglob = Kglob;
Material.Bglob = Bglob;
Material.Pglob = Pglob_bi;
Material.Aglobinv = Aglobinv;
Material.Kglobinv = Kglobinv;
Material.Bglobinv = Bglobinv;

clear Aglob Kglob Bglob Pglob_bi Aglobinv Bglobinv Kglobinv

end


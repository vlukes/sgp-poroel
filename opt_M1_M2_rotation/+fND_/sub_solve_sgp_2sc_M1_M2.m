function [x,res,tvol,vol_vec,Hnew,cellType] = sub_solve_sgp_2sc_slow_no_precalc_2mat(x,x_old,cellType,dfH,H,Materials,penv,L,Algparam,tau)
                                              
res = 0;
m = size(x,1);

%% derivatives
dA = dfH.A;
dK = dfH.K;
dB = dfH.B;

%% expansion point
Abar = H.A;
Kbar = H.K;
Bbar = H.B;

dLA = Abar-L.A;
dAL = mtimesx(dA,dLA);
dLB = Bbar-L.B;
dBL = mtimesx(dB,dLB);
dLK = Kbar-L.K;
dKL = mtimesx(dK,dLK);

BBglobA = -mtimesx(dLA,dAL);
BBglobK = -mtimesx(dLK,dKL);
BBglobB = -mtimesx(dLB,dBL);

%%
if nargout > 3
    vol_vec = zeros(m,1);
end

if nargout > 4
    Hnew.A = zeros(6,6,m);
    Hnew.B = zeros(3,3,m);
    Hnew.K = zeros(3,3,m);
end


% m_grid_bisect = Material.nr_points;
% grid_bisect = 0:m_grid_bisect;

assert(Algparam.filtrho == 0);

nsamples_alpha = Materials.Material1.nr_points + 1;
angles = 0:pi/180:pi;
angles = angles';
nangles = size(angles,1);

nsamples_beta = Materials.Material2.nr_points + 1;

Ftmp_angles = zeros(nangles,1);
idx_angles = zeros(nangles,nangles);
Ftmp_alpha3 = zeros(nsamples_alpha,1);
idx_alpha3 = zeros(nsamples_alpha,1);
Ftmp_alpha1 = zeros(nsamples_alpha,1);
idx_alpha1 = zeros(nsamples_alpha,1);
Ftmp_e = zeros(m,1);
idx_e = zeros(m,1);

min_idx = zeros(m,3);

Ftmp_beta = zeros(nsamples_beta,1);

x1 = zeros(m,1);
x2 = zeros(m,1);
x3 = zeros(m,1);
x4 = zeros(m,1);

glob_alpha = zeros(m,nsamples_alpha,nsamples_alpha,nangles);
glob_beta = zeros(m,nsamples_beta);

for e = 1:m
  xbar = x_old(e,2:4);
  xtest = zeros(3,1);
  for i = 1:nsamples_alpha
    for j = 1:nsamples_alpha
      Ainv = squeeze(Materials.Material1.Aglobinv(i,j,:,:));
      Binv = squeeze(Materials.Material1.Bglobinv(i,j,:,:));
      Kinv = squeeze(Materials.Material1.Kglobinv(i,j,:,:));
      vol_e = Materials.Material1.Pglob(i,j);
      xtest(1) = Materials.Material1.alpha1(i,j);
      xtest(2) = Materials.Material1.alpha3(i,j);
      for k = 1:nangles
        xtest(3) = angles(k);
        [Qm, Qt] = get_rotation_matrices(-angles(k));
        Arotinv = Qt' * Ainv * Qt;
        Brotinv = Qm' * Binv * Qm;
        Krotinv = Qm' * Kinv * Qm;
        res = sum(sum( BBglobA(:,:,e).* Arotinv)) + sum(sum( BBglobB(:,:,e).* Brotinv)) + sum(sum( BBglobK(:,:,e).* Krotinv)) + penv * vol_e;
        % penalize distance between stiffness matrices
        % penalize distance between stiffness matrices
        [Qm, Qt] = get_rotation_matrices(angles(k));
        A = Qt*squeeze(Materials.Material1.Aglob(i,j,:,:))*Qt';
        glob = 0.5*tau*sum((A - Abar(:,:,e)).^2, 'all');
        Ftmp_angles(k) = res + glob;
      end % angle
      % winner among all angles
      % Fmodel = Ftmp_angles+(Jvar(1)-Ftmp_angles(1));
      [res,idxmin] = min(Ftmp_angles);
      Ftmp_alpha3(j) = res ;%+ penv * vol_e + 0.5*tau*norm(xbar-xtest)^2;
      idx_angles(i,j) = idxmin;
    end % alpha3
    % winner among all alpha3 samples
    [res,idxmin] = min(Ftmp_alpha3);
    Ftmp_alpha1(i) = res;% + penv * Material.Pglob(i,idxmin) + 0.5*tau*norm(xbar-xtest)^2;
    idx_alpha3(i) = idxmin;
  end % alpha1
  [min_alpha,min_alpha_idx] = min(Ftmp_alpha1);
%   Ftmp_e(e) = res;
  idx_alpha1(e) = min_alpha_idx;
  % index to best alpha1 
  min_idx(e,1) = min_alpha_idx;
  % index to best alpha3
  min_idx(e,2) = idx_alpha3(min_alpha_idx);
  % index to best angle
  min_idx(e,3) = idx_angles(min_idx(e,1),min_idx(e,2));
  
  for i = 1:nsamples_beta
    Ainv = squeeze(Materials.Material2.Aglobinv(i,:,:));
    Binv = squeeze(Materials.Material2.Bglobinv(i,:,:));
    Kinv = squeeze(Materials.Material2.Kglobinv(i,:,:));
    vol_e = Materials.Material2.Pglob(i);
    res =  sum(sum( BBglobA(:,:,e).* Ainv)) + sum(sum( BBglobB(:,:,e).* Binv)) + sum(sum( BBglobK(:,:,e).* Kinv)) + penv * vol_e;
    % penalize distance between stiffness matrices
    A = squeeze(Materials.Material2.Aglob(i,:,:));
    glob = 0.5*tau*sum((A - Abar(:,:,e)).^2, 'all');
    glob_beta(e,i) = glob/tau;
    Ftmp_beta(i) = res + glob;
  end
  % minimum for cell type with void inclusion
  [min_beta,min_beta_idx] = min(Ftmp_beta);
  
  if min_alpha < min_beta
    cellType(e) = 0;
    i = min_idx(e,1);
    j = min_idx(e,2);
    k = min_idx(e,3);
    x2(e) = Materials.Material1.alpha1(i,j);
    x3(e) = Materials.Material1.alpha3(i,j);
    x4(e) = angles(k);
    [Qm, Qt] = get_rotation_matrices(x4(e));
    Anew = Qt * squeeze(Materials.Material1.Aglob(i,j,:,:)) * Qt';
    Knew = Qm * squeeze(Materials.Material1.Kglob(i,j,:,:)) * Qm';
    Bnew = Qm * squeeze(Materials.Material1.Bglob(i,j,:,:)) * Qm';
    vol_new = Materials.Material1.Pglob(i,j);
  else
    cellType(e) = 1;
    x1(e) = Materials.Material2.beta(min_beta_idx);
    Anew = squeeze(Materials.Material2.Aglob(min_beta_idx,:,:));
    Knew = squeeze(Materials.Material2.Kglob(min_beta_idx,:,:));
    Bnew = squeeze(Materials.Material2.Bglob(min_beta_idx,:,:));
    vol_new = Materials.Material2.Pglob(min_beta_idx);
  end
  
  Hnew.A(:,:,e) = Anew;
  Hnew.K(:,:,e) = Knew;
  Hnew.B(:,:,e) = Bnew;
  vol_vec(e) = vol_new;
end % elements


tvol = sum(vol_vec);

x(:,1) = x1;
x(:,2) = x2;
x(:,3) = x3;
x(:,4) = x4;


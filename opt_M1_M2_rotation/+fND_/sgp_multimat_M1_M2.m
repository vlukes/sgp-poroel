function [x,cellType,ppen] = sgp_multimat_debug_parfor(Geo,Material1,Material2,cellType,Algparam,itmax,penfl,rad,x0,k0,p0,lambda_phi,lambda_psi,sfepyFile)
% sequential global programming
%
% sgp_multimat(Geo,Material,Algparam,itmax,psimp,penfl,rad,x0,k0,penbw)
%
% INPUT:
%   Geo:      geometry struct
%       - name:     name of the problem xml file and output files
%       - noe:      number of elements
%       - nx:       number of elements in x-direction (only in the non-cfs)
%       - ny:       number of elements in y-direction (only in the non-cfs)
%       - nz:       number of elements in z-direction (only in the non-cfs)
%       - sx:       length in x-direction (only in the non-cfs)
%       - sy:       length in y-direction (only in the non-cfs)
%       - sz:       length in z-direction (only in the non-cfs)
%       - dim:  dimension
%   Material1: material struct for 1st material
%   Material2: material struct for 2nd material
%   Algparam: algorithmical parameters
%   itmax:    maximaliteration number
%   vvol:     desired volume
%   penfl:    filter penalization factor (not used currently)
%   rad:      filter radius
%   x0:       initial design
%   k0:       iteration number offset
%   p0:       initial penalty parameter for volume constraint
%   lambda_phi,lambda_psi:   J = lambda_phi * compliance + lambda_psi * permeability
%   sfepyFile: path to sfepy simulation file
% OUTPUT
%   x:        new design
%   celltype: flag list indicating which unit cell in which FE
%   ppen:     final penalty parameter for volume constraint

%% extract geometry
% number of elements
m = Geo.noe;

% dimension 
dim = Geo.dim;

% define volume fraction
vvol = Algparam.vvol;

total_volume = m;

% in matlab case only regular volume possible
% THIS HAS TO BE ADAPTED FOR IRREGULAR MESH IN SFEPY
Geo.elemVol = ones(Geo.noe,1);


siz = dim*(dim+1)/2;

x = x0;

%% algorithmic parameters 
% stopping criteria tolerance for iteration_history and bisection
% iteration
epsi = 1e-7;

% (inner) iteration bounds (volume constraint handeling)
nmaxbisections = 40;

% bool variable in order to set value of tau for the first time if
% neccesary
start_tau = 1;

% max number of proximal point updates
if Algparam.prox > 0
    nmaxproxupdates = 50;
else
    nmaxproxupdates = 1;
end

% initial proximal point factor
if Algparam.prox > 0 
    if penfl > 0
        tau = 1.0e-6;
    else 
        tau = 1e-6;
    end
else
   % prox term is turned off
   tau = 0; 
end


% setup volume constraint handeling constants
%
% INCREASE pmax if volume appears to be permanently too high!
%
if strcmp(Algparam.constraint_mode,'bisection') || strcmp(Algparam.constraint_mode,'dual')
    fac = 0.5;
    pmin = Algparam.pmin;
    pmax = Algparam.pmax;
    if p0 < 0
        ppen=fac*(pmax+pmin);
    else
        ppen = p0;
    end
    
    % deactivate bisection, if a fixed penalty parameter pmin=pmax is specified
    if pmin < pmax
        bSkipBisection = 0;
        bSkipDual = 0;
    else
        bSkipBisection = 1;
        bSkipDual = 0;
        vvol = 0;
    end
else
    error('penalty mode not implemented');
end


%% First evaluation of cost function, derivative, constraints, ...

% material interpolation
[H, vol_vec] = fND_.getMaterial_2sc(Geo,Material1,Material2,cellType,x);

% evaluate cost function and derivative with respect to tensor H

tttf1=clock;

% angles = 0:pi/180:pi;
% Jvar = zeros(size(angles,2),1);
% Jvar = plot_compliance(Jvar,angles,3,x,H,lambda_psi,lambda_phi,sfepyFile,vol_vec);

[J,DA,DK,DB,phi,psi] = call_sfepy_pe(H.A,H.K,H.B,lambda_phi,lambda_psi,sfepyFile,0,x,vol_vec);    
dfHm.A = DA;
dfHm.K = DK;
dfHm.B = DB;

tttf2=clock;
tttfd=etime(tttf2,tttf1);


%% filter current design (using A tensor or parameters ...)
% if rad > 1 && penfl > 0
%     if Algparam.filtrho == 1
%         ffilt = sum(sum((x(:,2:4)-xf).^2)); % compute squared distance to filtered design
%     else
%       throw("tensor filtering not implemented!");
% %         ffilt = sum(sum(sum((H.A-AF).^2))); % compute squared distance to filtered design
%     end
%     ffilt = .5*ffilt;             % regularization term using filtered design
% else
%     ffilt = 0;
% end

ffilt = 0;

fvol = sum(vol_vec);
%% calculate merit function
fmer = J + ppen * (fvol/total_volume-vvol)  + penfl*ffilt;

%% initial parameters for optimization loop
nbisec = 0;   % bisection counter

L.A = zeros(6,6,m); % lower asymptote A
L.B = zeros(3,3,m); % lower asymptote B
L.K = zeros(3,3,m); % lower asymptote K


% create .plot.dat file
fid = fopen(strcat(Geo.problem,'.txt'), 'a' );
        
fprintf(fid,'it     f_obj      vol      filt     merit      tau    ppen   time    timef   timesub  nsub phi psi\n');
fprintf(fid,'%4d %10.4e %f %8.2e %8.2e %10.4e %f %8.2e %8.2e %8.2e %3d %8.2e %8.2e\n',...
        k0,J,fvol/total_volume, ffilt, fmer, tau, ppen, 0, 0, 0, nbisec, phi, psi);
    
fprintf(1,'__________________________________________________________________________________________________________________________________\n');
fprintf(1,'it   |   f_obj    |   vol    |   filt   |   merit    |   tau    |   ppen   |   time   |   timef  | timesub  | nsub | phi  | psi\n');
fprintf(1,'______________________________________________________________________________________________________________________________________________________\n');
fprintf(1,'%4d | %10.4e | %f | %8.2e | %8.2e | %10.4e | %f | %8.2e | %8.2e | %8.2e | %3d | %8.2e | %8.2e \n',...
        k0,J,fvol/total_volume, ffilt, fmer, tau, ppen, tttfd, tttfd, 0, nbisec, phi, psi);


% iteration counter
k = k0;

x_new = x;
x_old = x;
rStopold = Inf;

%% outer iteration loop 
count_breaks = 0;
while k <= itmax
    
    fnew = inf; % new cost function value
    inner_it = 0; % inner iteration counter
    
    %tau = tau / 1.1; % update prox variable (to allow for less conservative steps)
    if Algparam.prox > 0
      tau = 1e-6;
    end
    k = k + 1;

    %% innter iteration loop (prox-point loop) 
    while fnew > fmer && inner_it < nmaxproxupdates % inner iteration: increase prox parameter until descent 
        
        ttt1=clock;
    
        inner_it = inner_it+1;
        
        tttsd=0;
        ttts1=clock;
        
        % solve sub problem to global optimality!
        Materials.Material1 = Material1;
        Materials.Material2 = Material2;
        [x_new,~,~,vol_vec,Hnew_sub,cellType] = fND_.sub_solve_sgp_2sc_M1_M2(x,x_old,cellType,dfHm,H,Materials,ppen,L,Algparam,tau);        
        
        ttts2=clock;
        tttsd=tttsd + etime(ttts2,ttts1); 
            
        if strcmp(Algparam.constraint_mode,'bisection')
            % prepare bisection for multiplier of resource constraint
            pmaxi = pmax;
            pmini = pmin;
        end
        
        if(pmin==pmax) 
            bSkipBisection = 1;
            vvol=0;
        end
        
        
        if strcmp(Algparam.constraint_mode,'dual') &&  bSkipDual == 0
            
            % currently not active ....
            error('dual method not supported for the moment ...');
            
        else
            
            rStop = 1.0e-3;   %max(min(.1*abs(fmer - fmerold)/abs(fmer),1.0e-5),1.0e-6);
            rStop = min(rStop,rStopold);
            rStopold = rStop;
            
            ncounter = 0; % (internal) bisection counter             
            
            %% bisection for volume equality constraint
            while bSkipBisection == 0
                %% calculate volume
                fvol = sum(vol_vec);
                vol = fvol/total_volume;
                
                ncounter = ncounter + 1;
                if abs(vol-vvol) < rStop || ncounter > nmaxbisections
                    bSkipBisection = 1;
                else
                    if vol > vvol
                        pmini = ppen;
                        ppen  = .5*(pmaxi+ppen);
                    else
                        pmaxi = ppen;
                        ppen  = .5*(pmini+ppen);
                    end

                end

                ttts1=clock;

                % re-solve sub problem to global optimality with updated multiplier for volume constraint!
                [x_new,~,~,vol_vec,Hnew_sub,cellType] = fND_.sub_solve_sgp_2sc_M1_M2(x,x_old,cellType,dfHm,H,Materials,ppen,L,Algparam,tau);        
                
                ttts2=clock;
                tttsd=tttsd + etime(ttts2,ttts1);

            end
        end
        
        %reset bisection variables
        nbisec = ncounter;
        if strcmp(Algparam.constraint_mode,'bisection')
            bSkipBisection = 0;
        end
        
        % update material interpolation
        
        % do nothing, available from sub problem!!
        H_new = Hnew_sub;
        
        tttf1=clock;
        
        %% update compliance c and derivative with respect to Y_new

        [J,DA,DK,DB,phi,psi] = call_sfepy_pe(H_new.A,H_new.K,H_new.B,lambda_phi,lambda_psi,sfepyFile,k,x_new,vol_vec);    
        dfHmtmp.A = DA;
        dfHmtmp.K = DK;
        dfHmtmp.B = DB;
        tttf2=clock;
        tttfd=etime(tttf2,tttf1);
        
        
        %% update volume
        fvol = sum(vol_vec);
        vol = fvol/total_volume;
        
        %% update merit function
        fnew = J + ppen * (fvol-vvol*total_volume);        
        
        %% Did we succeed in finding find a better point? If not, increase weight of
        % proximal term ...
        if fnew > fmer
            if k > 0 && Algparam.prox > 0
                % some output for inner iteration ...
                fprintf('tau %10.6e fnew=%f fmer = %f vol=%10.6e\n', tau, fnew, fmer, ...
                    vol);
                
                if start_tau == 1
                    if Algparam.prox > 0
                        tau=1e-6;
                    else
                        tau = 0;
                    end
                    start_tau = 0;
                end
                tau = tau * 1.5;
            else
                if k > 1 % 10
                    break;
                end
            end
        end

    end  % end of inner iteration
     
    if abs(fnew-fmer) <= 1e-6
      count_breaks = count_breaks + 1;
    end
    
    % stop algorithm if objective hasn't changed in the last few iterations
    if count_breaks == 2
      break;
    end
     
    %% update solution
    x_old = x;
    x = x_new;
    H = H_new;
    dfHm = dfHmtmp;
    fmer = fnew;

    %plotall_symm(x,Geo,.1);
%     plotall(x,Geo,Material,vol_vec,filt_vec);
             
    %save x_ x
    
    
    %% output and update iteration counter k
    ttt2=clock;
    % some output to screen ...
    fprintf(fid,'%4d %10.4e %f %8.2e %8.2e %10.4e %f %8.2e %8.2e %8.2e %3d %8.2e %8.2e\n',...
        k,J,vol, ffilt, fmer, tau, ppen, etime(ttt2,ttt1), tttfd, tttsd, nbisec, phi, psi);
    
%     if mod(k,20) == 0
%         fprintf(1,'_______________________________________________________________________________________________________________________________________________________\n');
%         fprintf(1,'it   |   f_obj    |   vol    |   filt   |   merit       |   tau    |   ppen   |   time   |   timef  | timesub  | nsub | phi | psi n');
%         fprintf(1,'_______________________________________________________________________________________________________________________________________________________\n');
%     end
    
    fprintf(1,'%4d | %10.4e | %f | %8.2e | %8.2e | %14.8e | %f | %8.2e | %8.2e | %8.2e | %3d | %8.2e | %8.2e \n',...
        k,J,vol, ffilt, fmer, tau, ppen, etime(ttt2,ttt1), tttfd, tttsd, nbisec, phi, psi);

end

% plotall(x,Geo,Material,vol_vec,filt_vec);
fprintf(1,'final design: fmer_last_min = %f\n',fmer);


fclose(fid);

end

function Jvar = plot_compliance(Jvar,angles,idx,x,H,lambda_psi,lambda_phi,sfepyFile,vol_vec)
  % index of element we want to vary
  cnt = 1;

  Aconst = [2.1417    0.7771    0.6568    0.0000    0.0000    0.0000
            0.7771    2.1417    0.6567    0.0000    0.0000    0.0000
            0.6568    0.6567    1.7919    0.0000    0.0000    0.0000
            0.0000    0.0000    0.0000    0.5904    0.0000    0.0000
            0.0000    0.0000    0.0000    0.0000    0.5904    0.0000
            0.0000    0.0000    0.0000    0.0000    0.0000    0.4866];
  
  Bconst = [ 0.0619    0.0000    0.0000
                  0    0.0619    0.0000
                  0         0    0.0669];
                
  Kconst = [9.6319   -0.0489   -0.0247
            -0.0489   9.6319    0.0282
          -0.0247     0.0282    2.6984];
  
  for a = angles
    % reset all tensors to intial design
    Aall = H.A;
    Ball = H.B;
    Kall = H.K;
    xall = x;
    
    [Qm, Qt] = get_rotation_matrices(a);
  
    % vary tensor of one element
    Arot = Qt*Aconst*Qt';
    Brot = Qm * Bconst * Qm';
    Krot = Qm * Kconst * Qm';
    % vary material tensors in one element
    Aall(:,:,idx) = Arot;
    Ball(:,:,idx) = Brot;
    Kall(:,:,idx) = Krot;
    xall(idx,4) = a;
    [J,DA,DK,DB,phi,psi] = call_sfepy_pe(Aall,Kall,Ball,lambda_phi,lambda_psi,sfepyFile,0,x,vol_vec);
    Jvar(cnt) = J;
    cnt = cnt + 1;
  end
  plot(angles,Jvar)
end
 

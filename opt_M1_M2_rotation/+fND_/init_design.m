function [x] = init_design(Material1,Material2,Geo,cellType,init_alpha1,init_alpha3, init_phi,init_beta)
% calculate initial design with (almost) zero variation from default config 
% input: Material, Geo structs with information about Material, Geo

% assert(init_alpha1 == 0 & init_alpha3 == 0 & init_phi == 0 | init_beta == 0);

alpha1 = unique(Material1.alpha1);
alpha3 = unique(Material1.alpha3);
% find closest value to given initial value for alpha1 and alpha3
[~, idx] = min(abs(init_alpha1 - alpha1));
init_alpha1 = alpha1(idx);

[~, idx] = min(abs(init_alpha3 - alpha3));
init_alpha3 = alpha3(idx);

phi_rand = 0;
if isstring(init_phi) && init_phi == "random"
  phi_rand = 1;
  init_phi_vec = 0+rand(Geo.noe,1)*pi;
end

% find closest value to given initial value for beta
beta = Material2.beta;
[~, idx] = min(abs(init_beta - beta));
x(:,1) = beta(idx);

fprintf(1, 'initial values: alpha1 = %8.2e   alpha3 = %8.2e   phi= %8.2e  beta = %8.2e\n', init_alpha1, init_alpha3, init_phi, init_beta);

x = zeros(Geo.noe,4);

if init_beta > 0
  % output: x  parameter vector 
  
end

for e = 1:Geo.noe
  % unit cell with 3 fluid channels
  if cellType(e) == 0
    x(e,2) = init_alpha1;
    x(e,3) = init_alpha3;
    if phi_rand
      x(e,4) = init_phi_vec(e);
    else  
      x(e,4) = init_phi;
    end
  else
    x(e,1) = init_beta;
  end
end

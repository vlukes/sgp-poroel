clearvars; close all;

% set paths
rootPath = pwd;
addpath(genpath('.'))
addpath(genpath('../utilities'))

% location of mtimesx
% can be downloaded from: https://de.mathworks.com/matlabcentral/fileexchange/25977-mtimesx-fast-matrix-multiply-with-multi-dimensional-support
addpath(genpath('/opt/mtimesx-master'))

% path to folder where all vtk output files are written
outDir = strcat(pwd, '/output');

if exist(outDir) == 7 
  rmdir(outDir, 's');
end 
 mkdir(outDir);

% geometric specifications
Geo.dim = 3; Geo.ny=2;Geo.nx=15;Geo.nz=10;
Geo.sx = 2; Geo.sy = 15; Geo.sz = 10;Geo.noe = Geo.ny*Geo.nx*Geo.nz;

% maximum number of (outer) optimization iterations
iterations = 30; 

% weighting factors for objective function: lambda_phi * \Phi + lambda_psi * \Psi
% \Phi: compliance, \Psi: fluid flux
lambda_phi = 1; lambda_psi = -10;

% location of sfepy driver file
sfepyFile = strcat("../utilities/pe_macro_scaled_15x10x2.py");

% location of material data files with members of A^\text{grid}_1 and A^\text{grid}_2:
% - A^\text{grid}_1: parameter samples from M_1 among which we search a minimizer/maximizer
% - A^\text{grid}_2: parameter samples from M_2 among which we search a minimizer/maximizer
matFile1 = strcat('../utilities/data_pe_param-3-angles_perr-pts-28_alpha13_noRot.mat');
matFile2 = strcat('../utilities/data_pe_param-1-inclusion_pts-60.mat');

% volume constraint on matrix phase, here: inactive
Algparam.vvol = 0; 
% no regularization
Algparam.filtrho = 0;

% name of log file
Geo.problem = sprintf('pe_%dx%dx%d_M1M2_lambda_%4.2f%4.2f',Geo.nx,Geo.ny,Geo.nz,lambda_phi,lambda_psi);

% initialize data structure for M1
load(matFile1,'x1','x2','nr_points');
Material1.perr_max = 1;
Material1.nr_points = nr_points;
Material1.nsamples_fine = Material1.nr_points^Material1.perr_max;
Material1.a = unique(x1);
Material1.b = unique(x2);
Material1.alpha1 = x1;
Material1.alpha3 = x2;

% initialize data structure for M2
load(matFile2,'x1','nr_points');
Material2.perr_max = 1;
Material2.nr_points = nr_points;
Material2.nsamples_fine = nr_points;
Material2.a = unique(x1);
Material2.beta = unique(x1);

% turn on globalization
Algparam.prox = 1;

% lower and upper bound for volume bisection
Algparam.pmin = 0; Algparam.pmax = 0;
Algparam.commit_stride = 1; Algparam.constraint_mode = 'bisection'; Algparam.subproblem_mode = 'debug'; Material1.matfile = 'dump';

% helper array: which cell type is currently active in which FE cell
% 0: M1, 1: M2
cellType = zeros(Geo.noe,1);

% extract info material data file and initialize start design
[Material1] = fND_.TwoScInit(Material1,Algparam,matFile1);
[Material2] = fND_.TwoScInit(Material2,Algparam,matFile2);
[x] = fND_.init_design(Material1, Material2, Geo, cellType, 0.22, 0.22, 0, 0.1);

xopt{1} = x;
fND_.runOptimization();

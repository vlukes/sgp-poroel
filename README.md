# Sequential Global Programming (SGP) for poroelastic applications

This project contains MATLAB source code that implements a Sequential Global Programming approach (SGP) for the multi-material optimization of two-scale settings. A description of the algorithm, as well as numerical results, can be found in the publication *New algorithm for solutions of multiphysics optimization
problems applied to the Biot porous media* by Vu, Stingl, Lukes, and Rohan (2023) - submitted to *Structural and Multidisciplinary Optimization*.

The three folders contain implementations for the following settings:
* opt_M1_rotation: optimization with only cells of type M1 + additional local orientation
* opt_M1_M2_rotation: optimization with both cells of type M1 and M2 + local orientation of M1 cells
* opt_M1_M2_rotation_reg: setting as in *opt_M1_M2_rotation* + regularization of the parameters of M1 and interface between M1 and M2 cells
In each of these folders, we provide a .m problem setting file with an examplary configuration for optimization parameters that can be used to reproduce the numerical results presented in the paper

The folder *utilities* contains files that are commonly used by each optimization setting (e.g. driver files for the finite element simulation with [SfePy], gradient computation, ...).


Requirements: 
* [mtimesx](https://de.mathworks.com/matlabcentral/fileexchange/25977-mtimesx-fast-matrix-multiply-with-multi-dimensional-support)
* [gmsh](https://gmsh.info) - an open source 3D finite element mesh generator
* [SfePy] - a finite element solver used for two-scale computation

[Link to preprint]()

[SfePy]: https://sfepy.org/doc-devel/index.html

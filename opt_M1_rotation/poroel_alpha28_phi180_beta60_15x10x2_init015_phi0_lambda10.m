clearvars; close all;
rootPath = pwd;
addpath(genpath('.'))
addpath(genpath('../utilities'))

% location of mtimesx
% can be downloaded from: https://de.mathworks.com/matlabcentral/fileexchange/25977-mtimesx-fast-matrix-multiply-with-multi-dimensional-support
addpath(genpath('/opt/mtimesx-master'))

% path to folder where all vtk output files are written
outDir = strcat(pwd, '/output');

if exist(outDir) == 7 
  rmdir(outDir, 's');
end 
 mkdir(outDir);

% geometric specifications
Geo.dim = 3; Geo.ny=2;Geo.nx=15;Geo.nz=10;
Geo.sx = 2; Geo.sy = 15; Geo.sz = 10;Geo.noe = Geo.ny*Geo.nx*Geo.nz;

% maximum number of (outer) optimization iterations
iterations = 30; 

% weighting factors for objective function: lambda_phi * \Phi + lambda_psi * \Psi
% \Phi: compliance, \Psi: fluid flux
lambda_phi = 1; lambda_psi = -10;

% location of sfepy driver file
sfepyFile = strcat("../utilities/pe_macro_scaled_15x10x2.py");

% location of file with members of A^\text{grid}_1: parameter samples from M_1 among which we search a minimizer/maximizer
matFile = strcat('../utilities/data_pe_param-3-angles_perr-pts-28_alpha13_noRot.mat');

% volume constraint on matrix phase, here: inactive
Material.vvol = 0; 

% name of log file
Geo.problem = sprintf('pe_%dx%dx%d_M1_lambda_%4.2f%4.2f',Geo.nx,Geo.ny,Geo.nz,lambda_phi,lambda_psi);

load(matFile,'x1','x2','perr_max','nr_points');

Material.perr_max = perr_max;
Material.nr_points = nr_points;
Material.a = unique(x1);
Material.b = unique(x2);
Material.alpha1 = x1; Material.alpha3 = x2;
Algparam.prox = 1;
Algparam.filtrho = 1;
Algparam.pmin = 0; Algparam.pmax = 0;

Algparam.commit_stride = 1; Algparam.constraint_mode = 'bisection'; Algparam.subproblem_mode = 'debug'; Material.matfile = 'dump';

% extract info material data file
[Material] = fND_.TwoScInit(Material,Algparam,matFile);

% initialize start design
[x] = fND_.init_design(Material,Geo, 0.15, 0.15, 0); xopt{1} = x;


fND_.runOptimization();

function [x,res,tvol,vol_vec, Hnew] = sub_solve_sgp_2sc_slow_no_precalc(x,x_old,dfH,H,Material,penv,L,Algparam,tau,penfl,dAF)

res = 0;

m = size(x,1);
Abar = H.A;
Kbar = H.K;
Bbar = H.B;
dA = dfH.A;
dK = dfH.K;
dB = dfH.B;
% perr = Material.perr_max;

dLA = Abar-L.A;
dAL = mtimesx(dA,dLA);
dLB = Bbar-L.B;
dBL = mtimesx(dB,dLB);
dLK = Kbar-L.K;
dKL = mtimesx(dK,dLK);

BBglobA = -mtimesx(dLA,dAL);
BBglobK = -mtimesx(dLK,dKL);
BBglobB = -mtimesx(dLB,dBL);

if nargout > 3
    vol_vec = zeros(m,1);
end

if nargout > 4
    Hnew.A = zeros(6,6,m);
    Hnew.B = zeros(3,3,m);
    Hnew.K = zeros(3,3,m);
end


% m_grid_bisect = Material.nr_points;
% grid_bisect = 0:m_grid_bisect;

dAFii = diag(dAF); % bei Uebergabe noch halbieren ...

if Algparam.filtrho == 1 && penfl > 0
    dAFiR1 = dAF*x(:,2)-x(:,2).*dAFii;
    dAFiR2 = dAF*x(:,3)-x(:,3).*dAFii;
    dAFiR3 = dAF*x(:,4)-x(:,4).*dAFii;
    %dAFiR_vec = [dAFiR1'; dAFiR2'; dAFiR3'];
else
  if penfl > 0
    % currently not active ....
    error('tensor regularization not supported for the moment ...');    
  end
end

nsamples = Material.nr_points + 1;
angles = 0:pi/60:pi;
angles = angles';
nangles = size(angles,1);

Ftmp_angles = zeros(nangles,1);
idx_angles = zeros(nsamples,nsamples);
Ftmp_alpha3 = zeros(nsamples,1);
idx_alpha3 = zeros(nsamples,1);
Ftmp_alpha1 = zeros(nsamples,1);
idx_alpha1 = zeros(nsamples,1);
Ftmp_e = zeros(m,1);
idx_e = zeros(m,1);

min_idx = zeros(m,3);

for e = 1:m
  xbar = x_old(e,:);
  xtest = zeros(4,1);
  for i = 1:nsamples
    for j = 1:nsamples
      Ainv = squeeze(Material.Aglobinv(i,j,:,:));
      Binv = squeeze(Material.Bglobinv(i,j,:,:));
      Kinv = squeeze(Material.Kglobinv(i,j,:,:));
      vol_e = Material.Pglob(i,j);
      xtest(2) = Material.alpha1(i,j);
      xtest(3) = Material.alpha3(i,j);
      for k = 1:nangles
        xtest(4) = angles(k);
        [Qm, Qt] = get_rotation_matrices(-angles(k));
        Arotinv = Qt' * Ainv * Qt;
        Brotinv = Qm' * Binv * Qm;
        Krotinv = Qm' * Kinv * Qm;
        res = sum(sum( BBglobA(:,:,e).* Arotinv)) + sum(sum( BBglobB(:,:,e).* Brotinv)) + sum(sum( BBglobK(:,:,e).* Krotinv));
        [Qm, Qt] = get_rotation_matrices(angles(k));
        A = Qt*squeeze(Material.Aglob(i,j,:,:))*Qt';
        glob = 0.5*tau*sum((A - Abar(:,:,e)).^2, 'all');
        Ftmp_angles(k) = res + glob;
      end % angle
      % winner among all angles
      [res,idxmin] = min(Ftmp_angles);
      Ftmp_alpha3(j) = res;
      idx_angles(i,j) = idxmin;
    end % alpha3
    % winner among all alpha3 samples
    [res,idxmin] = min(Ftmp_alpha3);
    Ftmp_alpha1(i) = res;
    idx_alpha3(i) = idxmin;
  end % alpha1
  [res,idxmin] = min(Ftmp_alpha1);
  Ftmp_e(e) = res;
  idx_alpha1(e) = idxmin;
  % index to best alpha1 
  min_idx(e,1) = idxmin;
  % index to best alpha3
  min_idx(e,2) = idx_alpha3(idxmin);
  % index to best angle
  min_idx(e,3) = idx_angles(min_idx(e,1),min_idx(e,2));
end % elements

x1 = zeros(m,1);
x2 = zeros(m,1);
x3 = zeros(m,1);
x4 = zeros(m,1);
for e = 1:m
  i = min_idx(e,1);
  j = min_idx(e,2);
  k = min_idx(e,3);
  x2(e) = Material.alpha1(i,j);
  x3(e) = Material.alpha3(i,j);
  x4(e) = angles(k);
  [Qm, Qt] = get_rotation_matrices(x4(e));
  Hnew.A(:,:,e) = Qt * squeeze(Material.Aglob(i,j,:,:)) * Qt';
  Hnew.K(:,:,e) = Qm * squeeze(Material.Kglob(i,j,:,:)) * Qm';
  Hnew.B(:,:,e) = Qm * squeeze(Material.Bglob(i,j,:,:)) * Qm';
  vol_vec(e) = Material.Pglob(i,j);
end

tvol = sum(vol_vec);

x(:,1)=x1;
x(:,2)=x2;
x(:,3)=x3;
x(:,4)=x4;


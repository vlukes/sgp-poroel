function [x] = init_design(Material,Geo,init_alpha1,init_alpha3,init_phi)
% calculate initial design with (almost) zero variation from default config 
% input: Material, Geo structs with information about Material, Geo
% output: x  parameter vector 

x = zeros(Geo.noe,4);
x(:,1) = 0.; % reserved for rotations, we don't use this!

alpha1 = unique(Material.alpha1);
alpha3 = unique(Material.alpha3);
% find closest value to given initial value for alpha1 and alpha3
[~, idx] = min(abs(init_alpha1 - alpha1));
init_alpha1 = alpha1(idx);

[~, idx] = min(abs(init_alpha3 - alpha3));
init_alpha3 = alpha3(idx);

fprintf(1, 'initial values: alpha1 = %8.2e   alpha3 = %8.2e   phi= %8.2e\n', init_alpha1, init_alpha3, init_phi);

x(:,2) = init_alpha1;
x(:,3) = init_alpha3;
if isstring(init_phi) && init_phi == "random"
  x(:,4) = 0+rand(Geo.noe,1)*pi;
else
  x(:,4) = init_phi;
end
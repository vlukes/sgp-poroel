function [H, vol_vec] = getMaterial_2sc(Geo,Material,x)
% this function computes the material stiffness for given design
% 
% Y = getMaterial(Geo, Material, x, scale)
%
% INPUT:
%   Geo: geometry struct, eg. Geo.noe: number of finite elements
%   Material: material struct
%          - A,B,K:  material interpolation coefficients from material
%          - a,b,c:    discretization of each design variable in material catalog
%   x:  design x(:,1) rotational angle, x(:,2:4) thickness design parameter
%   scale: optional scaling of material tensor, default: 1
%
% OUTPUT
%   H:        containing fields A, B and K
%
nelem = Geo.noe;
H.A = zeros(6,6,nelem);
H.K = zeros(3,3,nelem);
H.B = zeros(3,3,nelem);

% assume homogeneous initial design for alpha1 and alpha3
[rows1, cols1] = find(Material.alpha1 == x(1,2));
assert(size(rows1,1) > 1 && size(cols1,1) > 1);
[rows3, cols3] = find(Material.alpha3 == x(1,3));
assert(size(rows3,1) > 1 && size(cols3,1) > 1);

% find indices where both row/column indices match
row_idx = find(rows1 == rows3);
col_idx = find(cols1 == cols3);

% these are the indices to obtain respective material tensors for the initial values
row_idx = rows1(row_idx);
col_idx = cols1(col_idx);

% assume homogeneous design
Aconst = squeeze(Material.Aglob(row_idx,col_idx,:,:));
Bconst = squeeze(Material.Bglob(row_idx,col_idx,:,:));
Kconst = squeeze(Material.Kglob(row_idx,col_idx,:,:));
vol_vec = ones(nelem,1) * Material.Pglob(row_idx,col_idx);
for e = 1:nelem
  % set initial design
  [Qm, Qt] = get_rotation_matrices(x(e,4));
  H.A(:,:,e) = Qt * Aconst * Qt';
  H.B(:,:,e) = Qm * Bconst * Qm';
  H.K(:,:,e) = Qm * Kconst * Qm';
end

end
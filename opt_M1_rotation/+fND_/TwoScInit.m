function [Material] = TwoScInit(Material,Algparam,matFile)

Material.x_fine = Material.a; 
nsamples_fine = size(Material.a,2);

% if not available, call my_interp ... in directory stuff first ...
% load data_perr3_pe;
assert(isfile(matFile));
load(matFile);
% load data_pe_param-3-angles_perr-pts-2-lvl-1_alpha13.mat;

%recompute some info to use more precise model ... (attention: volume function has to be adapted to base cell ...)
  

X3 = zeros(nsamples_fine,1);
PPenf = zeros(nsamples_fine,1);
for k=1:nsamples_fine
  %% Pglob(i,j,k) = f3D_.truevolume(x(i),x(j),x(k)); % for analytic resourse function
  PPenf(k) = 0; %f3D_.penalize0(x(i),x(j),x(k),Algparam.thres);
  %             X1(i,j,k) = x(i);
  %             X2(i,j,k) = x(j);
  X3(k) = angle(k);
end

% Material.A11i = Aglobinv(:,:,1,1);
% Material.A22i = Aglobinv(:,:,2,2);
% Material.A33i = Aglobinv(:,:,3,3);
% Material.A44i = Aglobinv(:,:,4,4);
% Material.A55i = Aglobinv(:,:,5,5);
% Material.A66i = Aglobinv(:,:,6,6);
% Material.A12i = Aglobinv(:,:,1,2);
% Material.A13i = Aglobinv(:,:,1,3);
% Material.A23i = Aglobinv(:,:,2,3);
% 
% Material.K11i = Kglobinv(:,:,1,1);
% Material.K22i = Kglobinv(:,:,2,2);
% Material.K33i = Kglobinv(:,:,3,3);
% Material.K12i = Kglobinv(:,:,1,2);
% Material.K13i = Kglobinv(:,:,1,3);
% Material.K23i = Kglobinv(:,:,2,3);
% 
% Material.B11i = Bglobinv(:,:,1,1);
% Material.B22i = Bglobinv(:,:,2,2);
% Material.B33i = Bglobinv(:,:,3,3);
% Material.B12i = Bglobinv(:,:,1,2);
% Material.B13i = Bglobinv(:,:,1,3);
% Material.B23i = Bglobinv(:,:,2,3);

Material.Pglob = Pglob_bi;
Material.Aglobinv = Aglobinv;
Material.Aglob = Aglob;
Material.Bglobinv = Bglobinv;
Material.Kglob = Kglob;
Material.Kglobinv = Kglobinv;
Material.Bglob = Bglob;

clear Aglob Kglob Bglob Pglob_bi Aglobinv Bglobinv Kglobinv

if 0 % no analztic rotations at the moment
    Material.Thetaglob = Thetaglob;
end

end


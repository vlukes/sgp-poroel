from __future__ import absolute_import
from sfepy import data_dir
from sfepy.base.base import nm
from sfepy.homogenization.coefficients import Coefficients
from sfepy.base.base import Struct, debug, output
from sfepy.discrete import Problem
import os.path as osp
from scipy.io import savemat, loadmat
import numpy as np

my_dir = data_dir + '/FMO/'


def coefs2qp(coefs, nqp, keys=None, cells=None):
    """
    Copy a single coefficients into all quadrature points.
    Produce the correct shape of the coefficients.
    """
    out = {}
    if keys is None:
        keys = coefs.keys()
    for k in keys:
        v = coefs[k]
        if type(v) not in [nm.ndarray, float]:
            continue
        if cells is not None:
            v = v[cells, ...]

        if len(v.shape) == 2:
            v = v[..., nm.newaxis]
        
        out[k] = nm.repeat(v, nqp, axis=0)

    return out


def get_homog(ts, coors, mode=None, problem=None, **kwargs):
    """
    Material function. It returns the homog. coefficients
    in the quadrature points (coors).
    """
    if mode == 'qp':
        cf = problem.coefs
        term = kwargs['term']
        # adapted to Python 3
        nqp = list(term.integral.qps.values())[0].n_point
        region = term.region
        ient = len(region.can) - 1 - region.can[::-1].index(True)
        idxs = region.entities[ient]
        nel = idxs.shape[0]
        if region.name != 'Omega':
            if not region.can[-1] and region.can[-2]:
                idxs = region.get_facet_indices()[:, 0]

        assert(nqp * nel == coors.shape[0])
    
        return coefs2qp(cf, nqp, keys=['A', 'B', 'K'], cells=idxs)


def write_state_to_vtk(pb, state, flag='', sub_dir='./'):
    """
    Export state variables to the VTK file. 
    """
    base_name = osp.join(pb.output_dir, sub_dir, osp.split(pb.domain.name)[-1])
    
    velocity = pb.evaluate('ev_diffusion_velocity.i.Omega(hom.K, p)',
                           mode='el_avg')
    
    cellIds = []
    if 'design' in pb.coefs:
      # design has dimensions nel x 4
      # parameters for inclusion cell
      beta = nm.reshape(pb.coefs['design'][:,0],(-1,1,1,1))
      
      # parameters for cross cell
      des_x = nm.reshape(pb.coefs['design'][:,1],(-1,1,1))
      des_y = nm.reshape(pb.coefs['design'][:,2],(-1,1,1))
      des_z = nm.reshape(pb.coefs['design'][:,3],(-1,1,1))
    
      #in the end design must have dimensions (nelems,1,3,1)
      alpha = nm.stack((des_x,des_y,des_z),axis=2)
      nel = des_x.shape[0]
      
      cellIds = np.array(range(nel))
      # this can be done with one command in python3
      cellIds = np.expand_dims(cellIds,axis=1)
      cellIds = np.expand_dims(cellIds,axis=2)
      cellIds = np.expand_dims(cellIds,axis=3)
      
    if 'A' in pb.coefs:
      # per element, need a vector with 9 values: a11, a12, a22, a13, a23, a33, a44, a55, a66
      a11 = np.expand_dims(pb.coefs['A'][:,0,0],axis=1)
      a12 = np.expand_dims(pb.coefs['A'][:,0,1],axis=1)
      a22 = np.expand_dims(pb.coefs['A'][:,1,1],axis=1)
      a13 = np.expand_dims(pb.coefs['A'][:,0,2],axis=1)
      a23 = np.expand_dims(pb.coefs['A'][:,1,2],axis=1)
      a33 = np.expand_dims(pb.coefs['A'][:,2,2],axis=1)
      a44 = np.expand_dims(pb.coefs['A'][:,3,3],axis=1)
      a55 = np.expand_dims(pb.coefs['A'][:,4,4],axis=1)
      a66 = np.expand_dims(pb.coefs['A'][:,5,5],axis=1)
      
      #stiffness = nm.stack((a11, a12, a22, a13, a23, a33, a44, a55, a66),axis=2)
      stiffness = nm.stack((a11, a22, a33, a12, a23, a13, a44, a55, a66),axis=2)
      stiffness = np.expand_dims(stiffness,axis=3)
      
    if 'R' in pb.coefs:
      # regularization vector R with normalized values of cross cell (or -1 if inclusion cell) of size (nelem,3)
      R_1 = nm.reshape(pb.coefs['R'][:,0],(-1,1,1))
      R_2 = nm.reshape(pb.coefs['R'][:,1],(-1,1,1))
      R_3 = nm.reshape(pb.coefs['R'][:,2],(-1,1,1))
      R = nm.stack((R_1,R_2,R_3),axis=2)  
    
    if 'R_filt' in pb.coefs:
      # regularization vector R with normalized values of cross cell (or -1 if inclusion cell) of size (nelem,3)
      R_filt_1 = nm.reshape(pb.coefs['R_filt'][:,0],(-1,1,1))
      R_filt_2 = nm.reshape(pb.coefs['R_filt'][:,1],(-1,1,1))
      R_filt_3 = nm.reshape(pb.coefs['R_filt'][:,2],(-1,1,1))
      R_filt = nm.stack((R_filt_1,R_filt_2,R_filt_3),axis=2)
      
    if 'reg_comp' in pb.coefs:
      # regularization vector R with normalized values of cross cell (or -1 if inclusion cell) of size (nelem,3)
      reg_comp_1 = nm.reshape(pb.coefs['reg_comp'][:,0],(-1,1,1))
      reg_comp_2 = nm.reshape(pb.coefs['reg_comp'][:,1],(-1,1,1))
      reg_comp_3 = nm.reshape(pb.coefs['reg_comp'][:,2],(-1,1,1))
      reg_comp = nm.stack((reg_comp_1,reg_comp_2,reg_comp_3),axis=2)  
      
    vtkout = {}
    nnd, dim = pb.domain.mesh.coors.shape
    
    vtkout['u'] = Struct(name='output_data',
                         mode='vertex',
                         dofs=None,
                         var_name='displ',
                         data=state['u'].reshape((nnd, dim)))
    vtkout['p'] = Struct(name='output_data',
                         mode='vertex',
                         dofs=None,
                         var_name='press',
                         data=state['p'].reshape((nnd, 1)))
    vtkout['w'] = Struct(name='output_data',
                         mode='cell',
                         dofs=None,
                         var_name='velocity',
                         data=velocity)
    vtkout['cellId'] = Struct(name='output_data',
                         mode='cell',
                         dofs=None,
                         var_name='cellId',
                         data=cellIds)
    if 'design' in pb.coefs:
      vtkout['alpha'] = Struct(name='output_data',
                          mode='cell',
                          dofs=None,
                          var_name='alpha',
                          data=alpha)
      vtkout['beta'] = Struct(name='output_data',
                          mode='cell',
                          dofs=None,
                          var_name='beta',
                          data=beta)
    if 'K' in pb.coefs:
      vtkout['permeability'] = Struct(name='output_data',
                          mode='cell',
                          var_name='permeability',
                          data=np.expand_dims(pb.coefs['K'],axis=1))
    if 'A' in pb.coefs:
      vtkout['stiffness'] = Struct(name='output_data',
                          mode='cell',
                          var_name='stiffness',
                          data=stiffness)
      # strain energy
      strain_energy = pb.evaluate('dw_lin_elastic.i.Omega(hom.A, u, u)',
                                  mode='el_eval') * 0.5
      vtkout['strain_energy'] = Struct(name='output_data',
                                       mode='cell',
                                       data=strain_energy)

      # Cauchy stress
      cauchy_stress = pb.evaluate('ev_cauchy_stress.i.Omega(hom.A, u)',
                                  mode='el_eval')
      vtkout['cauchy_stress'] = Struct(name='output_data',
                                       mode='cell',
                                       data=cauchy_stress)
      
    if 'volume' in pb.coefs:   
      volume = nm.reshape(pb.coefs['volume'].flatten(),(-1,1,1,1))  
      vtkout['volume'] = Struct(name='output_data',
                         mode='cell',
                         dofs=None,
                         var_name='volume',
                         # must have shape nelems x 1 x 1 x 1
                         data=pb.coefs['volume'].reshape((-1,1,1,1)))
    
    # component wise R - F*R
    if 'reg_comp' in pb.coefs:
      vtkout['reg_comp'] = Struct(name='output_data',
                          mode='cell',
                          dofs=None,
                          var_name='reg_comp',
                          data=reg_comp) 
      
    if 'reg' in pb.coefs:   
      regularization = nm.reshape(pb.coefs['reg'].flatten(),(-1,1,1,1))  
      vtkout['reg_sum'] = Struct(name='output_data',
                         mode='cell',
                         dofs=None,
                         var_name='reg_sum',
                         # must have shape nelems x 1 x 1 x 1
                         data=pb.coefs['reg'].reshape((-1,1,1,1))) 
    if 'R' in pb.coefs:
      vtkout['R'] = Struct(name='output_data',
                          mode='cell',
                          dofs=None,
                          var_name='R',
                          data=R)  
    
    if 'R_filt' in pb.coefs:
      vtkout['R_filt'] = Struct(name='output_data',
                          mode='cell',
                          dofs=None,
                          var_name='R_filt',
                          data=R_filt)
      
    pb.domain.mesh.write(base_name + flag + '.vtk', out=vtkout)


def write_qpvals_to_vtk(pb, vals, flag=''):
    """
    Export values in quadrature points to the VTK file. 
    """

    vtkout = {}

    for k, v in vals.items():
        nqp = v.shape[1]
        for ii in range(nqp):
            vname = '%s_%d' % (k, ii + 1)
            sh = (v.shape[0], 1, v.shape[2], 1) 
            vtkout[vname] = Struct(name='output_data', mode='cell',
                                   var_name=vname,
                                   data=v[:, ii, ...].reshape(sh))
                                
    base_name = osp.join(pb.output_dir, osp.split(pb.domain.name)[-1])
    pb.domain.mesh.write(base_name + '_qp' + flag + '.vtk', out=vtkout)


def get_output_base(pb):
    return osp.join(pb.conf.options.output_dir,
                    osp.split(pb.domain.name)[-1])


def state_adjoint(pb):
    """
    Parametric hook: calculate state and adjoint states,
      evaluate tensors employed in the optimization. 
    """
    filename_coef = 'coefs_poroela_optim.mat'
    # filename_coef = 'coefs_poroela_optim_n.mat'
    filename_out_mat = 'macro_poroela_optim.mat'
    
    # define subdirectory for state solutions
    import os
    cwd = os.getcwd()
    state_dir = "state_solutions"
    state_dir_path = osp.join(pb.output_dir,state_dir)
    if not os.path.exists(state_dir_path):
      os.mkdir(state_dir_path)
      
    coefs = loadmat(osp.join(pb.output_dir, filename_coef))

    aux = [k for k in coefs.keys() if not k.startswith('__')]
    #print('loaded coefficients from "%s": %s' % (filename_coef, aux))
    ncoefs = coefs.get('ncoefs', 1)  # multiple state/adjoint problems?
    out_mat = {}
    # optimization iteration counter
    iteration = 0
    volume = []
    for icf in range(ncoefs):
        out = []

        if ncoefs == 1:
            assert('iteration' in coefs)
            # it's a value inside a list of a list
            iteration = coefs.pop('iteration')[0][0]
            assert('volume' in coefs)
            #volume = coefs.pop('volume')
            #volume = np.asarray(volume[0])).reshape()
            #print("volume:",(np.asarray(volume[0])).shape)
            pb.coefs = coefs
        else:
            pb.coefs = {k: v[icf] for k, v in coefs.items()
                        if k in ['A', 'B', 'K']}

        assert(iteration >= 0)
        # state problem
        output('>>> State problem <<<')

        pbvars = pb.get_variables()

        p_bar = nm.zeros((pb.domain.mesh.n_nod,), dtype=nm.float64)
        p_bar[pb.domain.regions['Gamma_P1'].entities[0]] = pb.conf.ebcs_p1
        p_bar[pb.domain.regions['Gamma_P2'].entities[0]] = pb.conf.ebcs_p2
        pbvars['P'].set_data(p_bar)

        yield pb, out
        
        #print(" out[-1][1]:",out[-1][1].variables)

        state = out[-1][1].get_parts()
        state['p'] += p_bar
        
        write_state_to_vtk(pb, state, flag='_st.' + str(iteration), sub_dir=state_dir)

        yield None

        # adjoint problem
        output('>>> Adjoint problem 1 <<<')

        conf = pb.conf.copy()
        conf.equations = equations_adj1
        ad_pb = Problem.from_conf(conf)
        ad_pbvars = ad_pb.get_variables()
        ad_pb.coefs = pb.coefs

        yield ad_pb, out


        ad_state1 = out[-1][1].get_parts()
        ad_state_vtk = {'u': ad_state1['u'], 'p': ad_state1['p']}

        #write_state_to_vtk(pb, ad_state_vtk, flag='_ad1')

        yield None

        output('>>> Adjoint problem 2 <<<')

        conf.equations = equations_adj2
        ad_pb = Problem.from_conf(conf)
        ad_pbvars = ad_pb.get_variables()
        ad_pb.coefs = pb.coefs

        p_tilde = nm.zeros((pb.domain.mesh.n_nod,), dtype=nm.float64)
        p_tilde[pb.domain.regions['Gamma_P2'].entities[0]] = 1
        ad_pbvars['P'].set_data(p_tilde)

        yield ad_pb, out

        ad_state2 = out[-1][1].get_parts()
        ad_state_vtk = {'u': ad_state2['u'], 'p': ad_state2['p'],
                        'p_tilde': p_tilde}

        #write_state_to_vtk(pb, ad_state_vtk, flag='_ad2')

        yield None

        mout = {}
        evvars = pb.get_variables()
        # evaluate objective functions
        evvars['u'].set_data(state['u'])
        mout['Phi'] = pb.evaluate('dw_surface_ltr.i.Gamma_N(force.val, u)')

        evvars['p'].set_data(state['p'])
        # mout['Psi'] = -pb.evaluate('d_surface_flux.i.Gamma_P2(hom.K, p)')
        evvars['P'].set_data(p_tilde)  # \tilde p
        mout['Psi'] = -pb.evaluate('dw_diffusion.i.Omega(hom.K, p, P)')

        mout['state_u'] = state['u'].reshape((-1, 3))
        mout['state_p'] = state['p'].reshape((-1, 1))
        #mout['adjoint_u'] = ad_state['u'].reshape((-1, 3))
        #mout['adjoint_p'] = ad_state['p'].reshape((-1, 1))


        # evaluate tensors
        evvars['u'].set_data(state['u'])
        eu = pb.evaluate('ev_cauchy_strain.i.Omega(u)', mode='qp')[..., 0]
        evvars['u'].set_data(ad_state1['u'])
        ev1 = pb.evaluate('ev_cauchy_strain.i.Omega(u)', mode='qp')[..., 0]
        evvars['u'].set_data(ad_state2['u'])
        ev2 = pb.evaluate('ev_cauchy_strain.i.Omega(u)', mode='qp')[..., 0]

        evvars['p'].set_data(state['p'])  # p + \bar p
        gp = pb.evaluate('ev_grad.i.Omega(p)', mode='qp')[..., 0]
        vp = pb.evaluate('ev_volume_integrate.i.Omega(p)', mode='qp')[..., 0]

        evvars['p'].set_data(ad_state1['p'])
        gq1 = pb.evaluate('ev_grad.i.Omega(p)', mode='qp')[..., 0]
        evvars['p'].set_data(ad_state2['p'])
        gq2 = pb.evaluate('ev_grad.i.Omega(p)', mode='qp')[..., 0]

        p_tilde = nm.zeros((pb.domain.mesh.n_nod,), dtype=nm.float64)
        #p_tilde[:] = nm.random.rand(pb.domain.mesh.n_nod)
        p_tilde[pb.domain.regions['Gamma_P1'].entities[0]] = 0
        p_tilde[pb.domain.regions['Gamma_P2'].entities[0]] = 1
        evvars['p'].set_data(p_tilde)  # \tilde p
        gtp = pb.evaluate('ev_grad.i.Omega(p)', mode='qp')[..., 0]

        pf = pb.fields['pressure']
        mapping, _ = pf.get_mapping(pb.domain.regions['Omega'],
                                    pb.integrals['i'], 'volume')

        det = mapping.det
        
        mout['eu_ev1'] = nm.sum(nm.einsum('ijk,ijl->ijkl', eu, ev1) * det, axis=1)
        mout['eu_ev2'] = nm.sum(nm.einsum('ijk,ijl->ijkl', eu, ev2) * det, axis=1)
        mout['gp_gq1'] = nm.sum(nm.einsum('ijk,ijl->ijkl', gp, gq1) * det, axis=1)
        mout['gp_gq2'] = nm.sum(nm.einsum('ijk,ijl->ijkl', gp, gq2) * det, axis=1)
        mout['gp_gtp'] = nm.sum(nm.einsum('ijk,ijl->ijkl', gp, gtp) * det, axis=1)
        mout['p_ev1'] = nm.sum((vp * ev1) * det[..., 0], axis=1)
        mout['p_ev2'] = nm.sum((vp * ev2) * det[..., 0], axis=1)
        mout['volume'] = mapping.volume

        write_qpvals_to_vtk(pb, {'eu': eu, 'ev1': ev1, 'ev2': ev2,
                                 'gp': gp, 'gq1': gq1, 'gq2': gq2,
                                 'qtp': gtp})

        if ncoefs == 1:
            out_mat = mout
        else:
            if len(out_mat) == 0:
                out_mat = {k: [] for k in mout.keys()}
            for k in mout.keys():
                out_mat[k].append(mout[k])

    savemat(osp.join(pb.output_dir, filename_out_mat), out_mat)


functions = {
    'get_homog': (get_homog,),  # material function - returns homog. coefs
}

mesh_dir = "./"
filename_mesh = mesh_dir + 'block_mesh_15x10x2.vtk'

regions = {
    'Omega': 'all',
    'Left': ('vertices in (x < 0.001)', 'facet'),
    'Gamma_N': ('vertices in (x > 0.999) & (y > 0.999)', 'facet'),
    'Gamma_P1': ('vertices in (x < 0.501) & (y > 0.999)', 'facet'),
    'Gamma_P2': ('vertices in (x > 1.499) & (y < 0.501)', 'facet'),
    #'Gamma_P2': ('vertices in (x > 1.499) & (y < 0.501) & (z < 0.101)', 'facet'),
}

materials = {
    'hom': 'get_homog',  # homog. coefs
     'force': ({'val': nm.array([[0, -10e0, 0]]).T},),  # traction force
#    'force': ({'val': nm.array([[0, -1e1, 0]]).T},),  # traction force
}

fields = {
    'displacement': ('real', 'vector', 'Omega', 1),
    'pressure': ('real', 'scalar', 'Omega', 1),
}

variables = {
    'u': ('unknown field', 'displacement'),
    'v': ('test field', 'displacement', 'u'),
    'p': ('unknown field', 'pressure'),
    'q': ('test field', 'pressure', 'p'),
    'P': ('parameter field', 'pressure', 'p'),
}

integrals = {
    'i': 2,
}

equations = {
    'balance_of_forces': """
        dw_lin_elastic.i.Omega(hom.A, v, u)
      - dw_biot.i.Omega(hom.B, v, p)
      = dw_surface_ltr.i.Gamma_N(force.val, v)
      + dw_biot.i.Omega(hom.B, v, P)""",
    'mass_conservation': """
        dw_diffusion.i.Omega(hom.K, q, p)
      =
      - dw_diffusion.i.Omega(hom.K, q, P)"""
}

equations_adj1 = {
    'balance_of_forces': """
        dw_lin_elastic.i.Omega(hom.A, v, u)
     = -dw_surface_ltr.i.Gamma_N(force.val, v)""",
    'mass_conservation': """
        dw_diffusion.i.Omega(hom.K, q, p)
      - dw_biot.i.Omega(hom.B, u, q)
      = 0"""
}

equations_adj2 = {
    'balance_of_forces': """
        dw_lin_elastic.i.Omega(hom.A, v, u)
     = 0""", #-dw_surface_ltr.i.Gamma_N(force.val, v)""",
    'mass_conservation': """
        dw_diffusion.i.Omega(hom.K, q, p)
      = dw_diffusion.i.Omega(hom.K, q, P)"""
}

#ebcs_p1 = 1e6
#ebcs_p2 = 0.5e6
ebcs_p1 = 1e0
ebcs_p2 = 0.5e0

ebcs = {
    'Fixed_u': ('Left', {'u.all': 0.0}),
    'Fixed_p1': ('Gamma_P1', {'p.0': 0.0}),
    'Fixed_p2': ('Gamma_P2', {'p.0': 0.0}),
}

solvers = {
    'ls': ('ls.scipy_direct', {}),
    'newton': ('nls.newton', {
        'i_max': 1,
        'eps_a': 1e-6,
    }),
}

options = {
    'output_dir': 'output',
    'parametric_hook': 'state_adjoint',
}

output.set_output(quiet=True);

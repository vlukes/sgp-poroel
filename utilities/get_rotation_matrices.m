function [Q_mat, Q_tens] = get_rotation_matrices(phi)
  global eps;
  % for numerical comparisons
  eps = 1e-10;
  
  Q_mat = get_rotation_matrix_3x3(phi);
  Q_tens = get_rotation_matrix_6x6(phi);
end

function R = get_rotation_matrix_3x3(phi)
% rotation order: first z-axis (gamma), then y-axis (beta), then x-axis (alpha)
  R = [cos(phi), -sin(phi), 0;
         sin(phi), cos(phi), 0;
         0, 0, 1];
end

%% rotation about z-axis (gamma)
function Q = get_rotation_matrix_6x6(phi)
   Q = [cos(phi)*cos(phi), sin(phi)*sin(phi), 0, 2*cos(phi)*sin(phi), 0, 0;
    sin(phi)*sin(phi), cos(phi)*cos(phi), 0, -2*cos(phi)*sin(phi), 0, 0;
    0, 0, 1, 0, 0, 0;
    -cos(phi)*sin(phi), cos(phi)*sin(phi), 0, cos(phi)*cos(phi) - sin(phi)*sin(phi), 0, 0;
    0, 0, 0, 0, cos(phi), -sin(phi);
    0, 0, 0, 0, sin(phi), cos(phi)];
end
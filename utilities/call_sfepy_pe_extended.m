function [J,DA,DK,DB,Phi,Psi] = call_sfepy_pe_extended(A,K,Bmat,lambda_phi,lambda_psi,sfepyFile,iteration,design,volume,R,R_filt,reg_comp,reg,fd)
  
 % B is a vector!!!

nelem = size(A,3);

if isempty(reg)
  reg = zeros(nelem,1);
  reg_comp = zeros(nelem,3);
end
if isempty(R)
  R = zeros(nelem,3);
end
if isempty(R_filt)
  R_filt = zeros(nelem,3);
end
% for finite difference check, the structure of input A,K,Bmat is a bit different
if nargin < 14
  fd = 0;
end

% check vectorization is necessary
if fd == 1% already vectorized
  B = Bmat;
else
  % vectorize Bmat
  B=zeros(nelem,6);
  for i=1:nelem
    B(i,:)=[Bmat(1,1,i),Bmat(2,2,i),Bmat(3,3,i),Bmat(1,2,i),Bmat(1,3,i),Bmat(2,3,i)];
  end

end
 
 % communicate current tensors to sfepy  ...
 % sfepy expects different ordering of A and K matrices
 if fd == 0
   A=permute(A,[3,1,2]);
   K=permute(K,[3,1,2]);
 end
 
 save(strcat(pwd,'/output/coefs_poroela_optim.mat'),'A','B','K','iteration','design','volume', 'R', 'R_filt', 'reg_comp', 'reg');
 
 if fd == 0
   A=permute(A,[2,3,1]);
   K=permute(K,[2,3,1]);
 end
 
 % call sfepy to evaluate obj and calc tensor derivatives
 assert(isfile(sfepyFile));
 system('simple.py ' + sfepyFile);

 % read obj and derivatives splitted into Phi and Psi terms
  
 load('./output/macro_poroela_optim.mat', 'Phi', 'Psi', 'eu_ev1', ...
     'gp_gtp', 'gp_gq1', 'gp_gq2', 'p_ev1');
 % NOTE: eu_ev1 == eu_ev2, p_ev1 == p_ev2, the NEW context!
 
 % calc cost
 J = lambda_phi*Phi + lambda_psi*Psi;
 
 if nargout > 1

     DA = zeros (6, 6, nelem);
     DK = zeros (3, 3, nelem);
     DB = zeros (3, 3, nelem);

     for elemtest=1:nelem
         DA1 = reshape(eu_ev1(elemtest,:,:),[6 6]);
         DA(:,:,elemtest) = lambda_phi*DA1; 
      
         DK0 = reshape(gp_gtp(elemtest,:,:),[3 3]);
         DK0 = .5*(DK0+DK0');
         DK1 = reshape(gp_gq1(elemtest,:,:),[3 3]);
         DK1 = .5*(DK1+DK1');
         DK2 = reshape(gp_gq2(elemtest,:,:),[3 3]);
         DK2 = .5*(DK2+DK2');
         DK(:,:,elemtest) = lambda_phi*DK1 +lambda_psi*(DK2 - DK0);

         DB1 = -reshape(p_ev1(elemtest,:,:),[6 1]);
         DBTMP = lambda_phi * DB1;
         DB(:,:,elemtest) = [DBTMP(1) DBTMP(4) DBTMP(5)
                             DBTMP(4) DBTMP(2) DBTMP(6)
                             DBTMP(5) DBTMP(6) DBTMP(3)];
     end 

 end

 
end


import numpy as nm
import os.path as osp
from sfepy.mechanics.matcoefs import stiffness_from_youngpoisson
from sfepy.homogenization.utils import define_box_regions
from sfepy.discrete.fem.mesh import Mesh
from sfepy.discrete import Problem
import sfepy.discrete.fem.periodic as per
import sfepy.homogenization.coefs_base as cb
from scipy.io import savemat
from pucgen import PUC

unit_cell_type = 2  # 1: 3D "corss", 2: sphere

filename_mesh = 'param_mesh.vtk'

wdir = osp.dirname(__file__)

per.set_accuracy(1e-4)

unit_cell_type_2 = {
    'mode': 'beta',
    'pucgen_data': "BaseCell;size=(1, 1, 1);el_size=0.05;mat_id=1\n"
                   "SphericalInclusion;radius=%f;central_point=(0, 0, 0);"
                   "el_size=1;mat_id=2",
    'pars': ([0.1, 0.4, 31],),  # [lower bound, upper bound, N]
    'fluid_is_connected': False,
    'matrix_id': 1,
    'fluid_id': 2,
}

unit_cell_type_1 = {
    'mode': 'alpha',
    'pucgen_data': "BaseCell;size=(1, 1, 1);el_size=0.05;mat_id=1\n"
                   "SphericalInclusion;radius=0.25;central_point=(0, 0, 0);"
                   "el_size=1;mat_id=2\n"
                   "CylindricalChannel;radius=%f;central_point=(0, 0, 0);"
                   "direction=y;el_size=1;mat_id=2\n"
                   "CylindricalChannel;radius=%f;central_point=(0, 0, 0);"
                   "direction=x;el_size=1;mat_id=2\n"
                   "CylindricalChannel;radius=0.15;central_point=(0, 0, 0);"
                   "direction=z;el_size=1;mat_id=2\n",
    'pars': ([0.08, 0.22, 3],
             [0.08, 0.22, 3]),  # [lower bound, upper bound, N]
    'fluid_is_connected': True,
    'matrix_id': 1,
    'fluid_id': 2,
}

unit_cell = {1: unit_cell_type_1, 2: unit_cell_type_2}[unit_cell_type]

fluid_is_connected = unit_cell['fluid_is_connected']
matrix_id, fluid_id = unit_cell['matrix_id'], unit_cell['fluid_id']


def gen_mesh(filename, par=None):
    filename_, _ = osp.splitext(filename)
    filename_puc = filename_ + '.puc'
    filename_vtk = filename_ + '.vtk'

    if par is None:
        par = (0.1, ) * len(unit_cell['pars'])

    with open(filename_puc, 'wt') as f:
        f.write(unit_cell['pucgen_data'] % tuple(par))

    puc = PUC.from_file(filename_puc)
    puc(filename_vtk, eps=1.)


def param_hook(pb):
    filename_mesh = osp.join(wdir, 'param_mesh.vtk')
    conf = pb.conf.copy()
    conf.filename_mesh = filename_mesh

    pars = []
    for lb, ub, N in unit_cell['pars']:
        pars.append(nm.linspace(lb, ub, N))

    if len(pars) == 1:
        pars = pars[0][:, None]
    elif len(pars) == 2:
        pars = nm.stack(nm.meshgrid(pars[0], pars[1]), -1).reshape(-1, 2)

    npars = pars.shape[0]

    sym = 3 * (dim - 1)
    cfA = nm.empty((npars, sym, sym))
    if fluid_id is not None:
        cfB = nm.empty((npars, sym, 1))
        cfPhi = nm.empty((npars, 1, 1))
        if fluid_is_connected:
            cfK = nm.empty((npars, dim, dim))

    out = []

    filename_mesh_aux = osp.join(pb.output_dir, 'mesh_%02d.vtk')
    for ii, par in enumerate(pars):
        print('>>>>>>>>>>>>>>>>>> %d, %s' % (ii, par))

        gen_mesh(filename_mesh, par)
        new_pb = Problem.from_conf(conf, init_equations=False)
        new_pb.force_init_he = True
        new_pb.domain.mesh.write(filename_mesh_aux % ii)

        yield new_pb, out

        coefs = out[-1][0]
        cfA[ii] = coefs.A
        if conf.fluid_id is not None:
            cfB[ii] = coefs.B.reshape((sym, 1))
            cfPhi[ii] = coefs.Phi
            if conf.fluid_is_connected:
                cfK[ii] = coefs.K
        yield None

    mode = unit_cell['mode']
    filename_database = osp.join(pb.output_dir, 'coefs_database_%s.mat')
    if conf.fluid_id is None:
        savemat(filename_database % mode, {'pars': pars, 'A': cfA})
    else:
        if fluid_is_connected:
            savemat(filename_database % mode,
                    {'pars': pars, 'A': cfA, 'B': cfB, 'K': cfK, 'phi': cfPhi})
        else:
            savemat(filename_database % mode,
                    {'pars': pars, 'A': cfA, 'B': cfB, 'phi': cfPhi})


filename_mesh = osp.join(wdir, filename_mesh)

gen_mesh(filename_mesh)

dim = 3

sym_eye = {
    2: 'nm.array([1, 1, 0])',
    3: 'nm.array([1, 1, 1, 0, 0, 0])',
}

per_tab = {
    2: [('x', 'left', 'right'), ('y', 'bottom', 'top')],
    3: [('x', 'left', 'right'), ('y', 'near', 'far'), ('z', 'bottom', 'top')],
}

regions = {
    'Y': 'copy r.Ym',
    'Gamma_Y': ('vertices of surface', 'facet'),
    # matrix
    'Ym': 'cells of group %d' % matrix_id,
    'Ym_left': ('r.Ym *s r.Left', 'facet'),
    'Ym_top': ('r.Ym *s r.Top', 'facet'),
    'Ym_bottom': ('r.Ym *s r.Bottom', 'facet'),
    'Ym_right': ('r.Ym *s r.Right', 'facet'),
}

if fluid_id is not None:
    regions.update({
        'Y': 'all',
        # channel
        'Yc': 'cells of group %d' % fluid_id,
        'Gamma_cm': ('r.Ym *s r.Yc', 'facet', 'Yc'),
        'Gamma_mc': ('r.Ym *s r.Yc', 'facet', 'Ym'),
        'Gamma_Ym0': ('r.Gamma_Y *s r.Ym', 'facet', 'Ym'),
        'Gamma_Ym': ('r.Gamma_Ym0 +s r.Gamma_mc', 'facet', 'Ym'),
    })

    if fluid_is_connected:
        regions.update({
            'Yc_left': ('r.Yc *s r.Left', 'facet'),
            'Yc_top': ('r.Yc *s r.Top', 'facet'),
            'Yc_bottom': ('r.Yc *s r.Bottom', 'facet'),
            'Yc_right': ('r.Yc *s r.Right', 'facet'),
        })

mesh = Mesh.from_file(osp.join(wdir, filename_mesh))
bbox = mesh.get_bounding_box()
regions.update(define_box_regions(dim, bbox[0], bbox[1]))
inter_data_one = nm.ones((mesh.n_nod, 1), dtype=nm.float64)

fields = {
    'volume': ('real', 'scalar', 'Y', 1),
    'displacement': ('real', 'vector', 'Ym', 1),
    'dvelocity': ('real', 'vector', 'Y', 1),
}

variables = {
    'u': ('unknown field', 'displacement'),
    'v': ('test field', 'displacement', 'u'),
    'Pi_u': ('parameter field', 'displacement', 'u'),
    'U1': ('parameter field', 'displacement', '(set-to-None)'),
    'U2': ('parameter field', 'displacement', '(set-to-None)'),
    'one': ('parameter field', 'volume', '(set-to-None)'),
}

if fluid_id is not None:
    fields.update({
        'velocity': ('real', 'vector', 'Yc', 2),
        'pressure': ('real', 'scalar', 'Yc', 1),
        'displacement_Yc': ('real', 'vector', 'Yc', 1),
    })

    variables.update({
        'w': ('unknown field', 'velocity'),
        'z': ('test field', 'velocity', 'w'),
        'p': ('unknown field', 'pressure'),
        'q': ('test field', 'pressure', 'p'),
        'Pi_w': ('parameter field', 'velocity', 'w'),
        'W1': ('parameter field', 'velocity', '(set-to-None)'),
        'W2': ('parameter field', 'velocity', '(set-to-None)'),
        'P1': ('parameter field', 'pressure', '(set-to-None)'),
    })

functions = {
    'match_x_plane': (per.match_x_plane,),
    'match_y_plane': (per.match_y_plane,),
    'match_z_plane': (per.match_z_plane,),
}

materials = {
    # Polystyrene (http://www.engineeringtoolbox.com/)
    'matrix': ({'D': stiffness_from_youngpoisson(dim, 3e9, 0.34)},),
    # Glycerine (http://www.engineeringtoolbox.com/)
    'fluid': ({'gamma': 1.0 / 4.35e9,  # 1 / bulk modulus
               'eta': 0.950,  # viscosity
               },),
}

ebcs = {
    'fixed_u': ('Corners', {'u.all': 0.0}),
}

if fluid_id is not None and fluid_is_connected:
    ebcs.update({
        'fixed_w': ('Gamma_cm', {'w.all': 0.0}),
    })

integrals = {
    'i1': 2,
    'i2': 3,
}

epbcs = {}

for d, p1, p2 in per_tab[dim]:
    epbcs.update({
        'per_u_' + d: (['Ym_' + p1, 'Ym_' + p2], {'u.all': 'u.all'},
                       'match_%s_plane' % d),
    })

    if fluid_id is not None and fluid_is_connected:
        epbcs.update({
            'per_p_' + d: (['Yc_' + p1, 'Yc_' + p2], {'p.0': 'p.0'},
                           'match_%s_plane' % d),
            'per_w_' + d: (['Yc_' + p1, 'Yc_' + p2], {'w.all': 'w.all'},
                           'match_%s_plane' % d),
        })

if dim == 2:
    regions.update({
        'Gamma_#': ('r.Top +s r.Bottom' +
                    ' +s r.Left +s r.Right', 'facet'),
    })
elif dim == 3:
    regions.update({
        'Ym_far': ('r.Ym *s r.Far', 'facet'),
        'Ym_near': ('r.Ym *s r.Near', 'facet'),
        'Gamma_#': ('r.Far +s r.Near' +
                    ' +s r.Left +s r.Right' +
                    ' +s r.Top +s r.Bottom', 'facet'),
    })

    if fluid_id is not None and fluid_is_connected:
        regions.update({
            'Yc_far': ('r.Yc *s r.Far', 'facet'),
            'Yc_near': ('r.Yc *s r.Near', 'facet'),
        })


all_periodic_u = ['per_u_%s' % ii for ii in ['x', 'y', 'z'][:dim]]
if fluid_id is not None and fluid_is_connected:
    all_periodic_p = ['per_p_%s' % ii for ii in ['x', 'y', 'z'][:dim]]
    all_periodic_w = ['per_w_%s' % ii for ii in ['x', 'y', 'z'][:dim]]

options = {
    'coefs': 'coefs',
    'coefs_filename': 'coefs_poroelastic',
    'requirements': 'requirements',
    'volume': {
        'variables': ['one'],
        'expression': 'd_volume.i1.Y(one)',
    },
    'output_dir': 'output',
    'return_all': True,
    'file_per_var': True,
    'parametric_hook': 'param_hook',
}

solvers = {
    'ls': ('ls.mumps', {}),
    'newton': ('nls.newton', {
        'i_max': 1,
        'eps_a': 1e-4,
        'eps_r': 1e-4,
        'problem': 'nonlinear',
    })
}

coefs = {
    'A': {
        'requires': ['pis_u', 'corrs_omega_rs'],
        'expression': 'dw_lin_elastic.i1.Ym(matrix.D, U1, U2)',
        'set_variables': [('U1', ('corrs_omega_rs', 'pis_u'), 'u'),
                          ('U2', ('corrs_omega_rs', 'pis_u'), 'u')],
        'class': cb.CoefSymSym,
    },
    'vol': {
        'regions': ['Ym'],
        'expression': 'd_volume.i1.%s(one)',
        'class': cb.VolumeFractions,
    },
    'filenames': {},
}

if fluid_id is not None:
    coefs.update({
        'C': {
            'status': 'auxiliary',
            'requires': ['corrs_omega_rs'],
            'expression': '- ev_div.i1.Ym(U1)',
            'set_variables': [('U1', 'corrs_omega_rs', 'u')],
            'class': cb.CoefSym,
        },
        'Phi': {
            'requires': ['c.vol'],
            'expression': 'c.vol["fraction_Yc"]',
            'class': cb.CoefEval,
        },
        'B': {
            'requires': ['c.Phi', 'c.C'],
            'expression': 'c.C + c.Phi * %s' % sym_eye[dim],
            'class': cb.CoefEval,
        },
        'N': {
            'status': 'auxiliary',
            'requires': ['corrs_omega_p'],
            'expression': 'dw_lin_elastic.i1.Ym(matrix.D, U1, U2)',
            'set_variables': [('U1', 'corrs_omega_p', 'u'),
                              ('U2', 'corrs_omega_p', 'u')],
            'class': cb.CoefOne,
            },
        'M': {
            'requires': ['c.Phi', 'c.N'],
            'expression': 'c.N + c.Phi * %e' % materials['fluid'][0]['gamma'],
            'class': cb.CoefEval,
            },
        'eta': {
            'expression': '%e' % materials['fluid'][0]['eta'],
            'class': cb.CoefEval,
            },
        'vol': {
            'regions': ['Ym', 'Yc'],
            'expression': 'd_volume.i1.%s(one)',
            'class': cb.VolumeFractions,
        },
    })

    if fluid_is_connected:
        coefs.update({
            'K': {
                'requires': ['corrs_psi'],
                'expression': 'dw_div_grad.i1.Yc(W1, W2)',
                'set_variables': [('W1', 'corrs_psi', 'w'),
                                  ('W2', 'corrs_psi', 'w')],
                'class': cb.CoefDimDim,
            },
        })

requirements = {
    'pis_u': {
        'variables': ['u'],
        'class': cb.ShapeDimDim,
    },
    'corrs_omega_rs': {
        'requires': ['pis_u'],
        'ebcs': ['fixed_u'],
        'epbcs': all_periodic_u,
        'is_linear': True,
        'equations': {
            'balance_of_forces':
                """dw_lin_elastic.i1.Ym(matrix.D, v, u)
               = - dw_lin_elastic.i1.Ym(matrix.D, v, Pi_u)"""
        },
        'set_variables': [('Pi_u', 'pis_u', 'u')],
        'class': cb.CorrDimDim,
        'save_name': 'corrs_omega_rs',
        'dump_variables': ['u'],
        'solvers': {'ls': 'ls'},
    },
}

if fluid_id is not None:
    requirements.update({
        'pis_w': {
            'variables': ['w'],
            'class': cb.OnesDim,
        },
        'corrs_omega_p': {
            'requires': [],
            'ebcs': ['fixed_u'],
            'epbcs': all_periodic_u,
            'equations': {
                'balance_of_forces':
                    """dw_lin_elastic.i1.Ym(matrix.D, v, u)
                    = dw_surface_ltr.i1.Gamma_Ym(v)"""
                },
            'class': cb.CorrOne,
            'save_name': 'corrs_omega_p',
            'dump_variables': ['u'],
            'solvers': {'ls': 'ls'},
        },
    })

    if fluid_is_connected:
        requirements.update({
            'corrs_psi': {
                'requires': ['pis_w'],
                'ebcs': ['fixed_w'],
                'epbcs': all_periodic_w + all_periodic_p,
                'is_linear': True,
                'equations': {
                    'balance_of_forces':
                        """dw_div_grad.i2.Yc(z, w)
                        - dw_stokes.i2.Yc(z, p)
                        = dw_volume_dot.i2.Yc(z, Pi_w)""",
                    'incompressibility':
                        """dw_stokes.i2.Yc(w, q)
                        = 0""",
                },
                'set_variables': [('Pi_w', 'pis_w', 'w')],
                'class': cb.CorrDim,
                'save_name': 'corrs_psi',
                'dump_variables': ['w', 'p'],
                'solvers': {'ls': 'ls'},
            },
        })

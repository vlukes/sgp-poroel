clearvars; close all;

% set paths
rootPath = pwd;
addpath(genpath('.'))
addpath(genpath('../utilities'))

% location of mtimesx
% can be downloaded from: https://de.mathworks.com/matlabcentral/fileexchange/25977-mtimesx-fast-matrix-multiply-with-multi-dimensional-support
addpath(genpath('/opt/mtimesx-master'))

% path to folder where all vtk output files are written
outDir = strcat(pwd, '/output');

% create empty output directory by deleting and creating it
% 7 indicates a directory
if exist(outDir) == 7
  rmdir(outDir, 's');
end
mkdir(outDir);

%% Geometry definition
Geo.dim = 3; Geo.ny=2;Geo.nx=15;Geo.nz=10;
Geo.sx = 2; Geo.sy = 15; Geo.sz = 10;Geo.noe = Geo.ny*Geo.nx*Geo.nz;

% weighting factor for permeability: J = compliance + lambda * permeability
lambda_phi = 1;
lambda_psi = -3;

% path to sfepy simulation file
sfepyFile =  strcat("../utilities/pe_macro_scaled_15x10x2_incl.py");

% volume constraint on matrix phase, here: inactive
Algparam.vvol = 0;  % no volume constraint ....
Algparam.pmin = 0;  % no volume term!
Algparam.pmax = 0;
Algparam.bisection = 0;
Algparam.nmaxbisections = 15;
if Algparam.vvol > 0
  Algparam.bisection = 1;
end

% do we want to optimize with two cells? if 0, only fluid channel cell is considered
Algparam.two_cells = 1;

% location of material data files with members of A^\text{grid}_1 and A^\text{grid}_2:
% - A^\text{grid}_1: parameter samples from M_1 among which we search a minimizer/maximizer
% - A^\text{grid}_2: parameter samples from M_2 among which we search a minimizer/maximizer
matFile1 = strcat('../utilities/data_pe_param-3-angles_perr-pts-28_alpha13_noRot.mat');
if Algparam.two_cells == 1
  matFile2 = strcat('../utilities/data_pe_param-1-inclusion_pts-60.mat');
else
  matFile2 = [];
end

% regularization parameters
Algparam.rad = 1.3;
% regularization penalty \Lambda_\Xi
Algparam.penfl = 0; 
Algparam.filtering = 0;
if Algparam.rad > 0 && Algparam.penfl > 0
  Algparam.filtering = 1;
end


% name of log file
Geo.problem = sprintf('pe_%dx%dx%d_M1M2_lambda_%4.2f%4.2f_rad%4.2f_penfl%4.2f',Geo.nx,Geo.ny,Geo.nz,lambda_phi,lambda_psi,Algparam.rad,Algparam.penfl);

%% setup bounds on parameterization and parameter for subproblem
% initialize data structure for M1
load(matFile1,'x1','x2','nr_points','perr_max');
% number of total levels
Material1.perr_max = perr_max;
% number of grid points on one level for thickness parameters
Material1.nr_points = nr_points;
Material1.nsamples_fine = Material1.nr_points^Material1.perr_max;
% get sampled values for respective variables
% alpha1
Material1.a = unique(x1);
% alpha3
Material1.b = unique(x2);
Material1.alpha1 = x1;
Material1.alpha3 = x2;

% initialize data structure for M2
if Algparam.two_cells == 1
  % 2nd material
  load(matFile2,'x1','nr_points');
  Material2.perr_max = 1;
  Material2.nr_points = nr_points;
  Material2.nsamples_fine = nr_points;
  Material2.a = unique(x1);
  Material2.beta = unique(x1);
end

%% Algorithmic parameters
% turn on/off proximal point term
Algparam.prox = 1;
Algparam.nmaxproxupdates = 20;
Algparam.tau0 = 0;
if Algparam.prox == 1
  Algparam.tau0 = 1e-6;
end

% every commit_stride iteration the output data is saved
Algparam.commit_stride = 1;

% only 'bisection' is implemented
% Algparam.constraint_mode = 'bisection';

% choose constraint mode 'debug' or 'release'
% Algparam.subproblem_mode = 'debug';

% Material1.matfile = 'dump';

%% initialize start design 
[Material1] = fND_.TwoScInit(Material1,Algparam,matFile1);
if Algparam.two_cells == 1
  [Material2] = fND_.TwoScInit(Material2,Algparam,matFile2);
else
  Material2 = [];
end

%% flag array indicating which cell array to use:
% 0: unit cell with 3 fluid channels
% 1: unit cell with void inclusion
cellType = zeros(Geo.noe,1);

% consistency check
if Algparam.two_cells == 1
  assert(all(cellType == 0));
end

%% init x with feasible design
Materials.Material1 = Material1;
Materials.Material2 = Material2;
[x] = fND_.init_design(Materials, Geo, cellType, 0.08, 0.08, 0, 0.1);

xopt{1} = x;

% maximum number of (outer) optimization iterations
iterations = 30;

fND_.runOptimization();

function [x,res,tvol,vol_vec,Hnew,cellType] = sub_solve_sgp_2sc_slow_no_precalc_2mat_reg(x,cellType,dfH,H,Materials,penv,Algparam,tau,Rbar,dAF)
res = 0;
nelems = size(x,1);

%% derivatives
dA = dfH.A;
dK = dfH.K;
dB = dfH.B;

%% expansion point
Abar = H.A;
Kbar = H.K;
Bbar = H.B;

L = Algparam.L;
dLA = Abar-L.A;
dAL = mtimesx(dA,dLA);
dLB = Bbar-L.B;
dBL = mtimesx(dB,dLB);
dLK = Kbar-L.K;
dKL = mtimesx(dK,dLK);

BBglobA = -mtimesx(dLA,dAL);
BBglobK = -mtimesx(dLK,dKL);
BBglobB = -mtimesx(dLB,dBL);

%%
if nargout > 3
    vol_vec = zeros(nelems,1);
end

if nargout > 4
    Hnew.A = zeros(6,6,nelems);
    Hnew.B = zeros(3,3,nelems);
    Hnew.K = zeros(3,3,nelems);
end

nsamples_alpha = Materials.Material1.nr_points + 1;
angles = 0:pi/60:pi;
angles = angles';
nangles = size(angles,1);

Ftmp_angles = zeros(nangles,1);
idx_angles = zeros(nangles,nangles);
Ftmp_alpha3 = zeros(nsamples_alpha,1);
idx_alpha3 = zeros(nsamples_alpha,1);
Ftmp_alpha1 = zeros(nsamples_alpha,1);
idx_alpha1 = zeros(nsamples_alpha,1);

min_idx = zeros(nelems,3);

if Algparam.two_cells == 1
  nsamples_beta = Materials.Material2.nr_points + 1;
  Ftmp_beta = zeros(nsamples_beta,1);
else
  nsamples_beta = 0;
  Ftmp_beta = [];
end

x1 = zeros(nelems,1);
x2 = zeros(nelems,1);
x3 = zeros(nelems,1);
x4 = zeros(nelems,1);

% prepare filtering
dAFii = zeros(nelems,1);
dAFiR1 = zeros(nelems,1);
dAFiR2 = zeros(nelems,1);
dAFiR3 = zeros(nelems,1);
if Algparam.filtering
  dAFii = diag(dAF); % bei Uebergabe noch halbieren ...
  dAFiR1 = dAF*Rbar(:,1) - Rbar(:,1).*dAFii;
  dAFiR2 = dAF*Rbar(:,2) - Rbar(:,2).*dAFii;
  dAFiR3 = dAF*Rbar(:,3) - Rbar(:,3).*dAFii;
end

% glob_alpha = zeros(nelems,nsamples_alpha,nsamples_alpha,nangles);
% glob_beta = zeros(nelems,nsamples_beta);

regs = zeros(nangles,1);

for e = 1:nelems
  for i = 1:nsamples_alpha
    for j = 1:nsamples_alpha
      Ainv = squeeze(Materials.Material1.Aglobinv(i,j,:,:));
      Binv = squeeze(Materials.Material1.Bglobinv(i,j,:,:));
      Kinv = squeeze(Materials.Material1.Kglobinv(i,j,:,:));
      vol_e = Materials.Material1.Pglob(i,j);
      for k = 1:nangles
        [Qm, Qt] = get_rotation_matrices(-angles(k));
        Arotinv = Qt' * Ainv * Qt;
        Brotinv = Qm' * Binv * Qm;
        Krotinv = Qm' * Kinv * Qm;
        res = sum(sum( BBglobA(:,:,e).* Arotinv)) + sum(sum( BBglobB(:,:,e).* Brotinv)) + sum(sum( BBglobK(:,:,e).* Krotinv)) + penv * vol_e;
        
        %% globalization term
        % penalize distance between stiffness matrices
        [Qm, Qt] = get_rotation_matrices(angles(k));
        A = Qt*squeeze(Materials.Material1.Aglob(i,j,:,:))*Qt';
        glob = 0.5*tau*sum((A - Abar(:,:,e)).^2, 'all');

        %% regularization term
        R = zeros(3,1);
        R(1) = (i-1)/(nsamples_alpha-1);
        R(2) = (j-1)/(nsamples_alpha-1);
        R(3) = cos(2*angles(k)-pi/2.0);
        RS = sum(R.^2);
        
        reg = Algparam.penfl * (dAFiR1(e)*R(1) + dAFiR2(e)*R(2) + dAFiR3(e)*R(3) + .5*dAFii(e)*RS);
        regs(k) = reg;
        Ftmp_angles(k) = res + glob + reg;
      end % angle
      % winner among all angles
      % Fmodel = Ftmp_angles+(Jvar(1)-Ftmp_angles(1));
      [res,idxmin] = min(Ftmp_angles);
      Ftmp_alpha3(j) = res ;
      idx_angles(i,j) = idxmin;
    end % alpha3
    % winner among all alpha3 samples
    [res,idxmin] = min(Ftmp_alpha3);
    Ftmp_alpha1(i) = res;
    idx_alpha3(i) = idxmin;
  end % alpha1
  [min_alpha,min_alpha_idx] = min(Ftmp_alpha1);
%   Ftmp_e(e) = res;
  idx_alpha1(e) = min_alpha_idx;
  % index to best alpha1 
  min_idx(e,1) = min_alpha_idx;
  % index to best alpha3
  min_idx(e,2) = idx_alpha3(min_alpha_idx);
  % index to best angle
  min_idx(e,3) = idx_angles(min_idx(e,1),min_idx(e,2));
  
  if Algparam.two_cells == 1
    for i = 1:nsamples_beta
      Ainv = squeeze(Materials.Material2.Aglobinv(i,:,:));
      Binv = squeeze(Materials.Material2.Bglobinv(i,:,:));
      Kinv = squeeze(Materials.Material2.Kglobinv(i,:,:));
      vol_e = Materials.Material2.Pglob(i);
      res =  sum(sum( BBglobA(:,:,e).* Ainv)) + sum(sum( BBglobB(:,:,e).* Binv)) + sum(sum( BBglobK(:,:,e).* Kinv)) + penv * vol_e;
  
      %% globalization term
      % penalize distance between stiffness matrices
      A = squeeze(Materials.Material2.Aglob(i,:,:));
      glob = 0.5*tau*sum((A - Abar(:,:,e)).^2, 'all');
  
      %% regularization term
      R = zeros(3,1);
      R(:) = -1;
      RS = sum(R.^2);
      
      reg = Algparam.penfl * (dAFiR1(e)*R(1) + dAFiR2(e)*R(2) + dAFiR3(e)*R(3) + .5*dAFii(e)*RS);
      Ftmp_beta(i) = res + glob + reg;
    end
    % minimum for cell type with void inclusion
    [min_beta,min_beta_idx] = min(Ftmp_beta);
  else
    min_beta = 9999; % fake undesired material
  end
  
  if min_alpha < min_beta
    cellType(e) = 0;
    i = min_idx(e,1);
    j = min_idx(e,2);
    k = min_idx(e,3);
    x2(e) = Materials.Material1.alpha1(i,j);
    x3(e) = Materials.Material1.alpha3(i,j);
    x4(e) = angles(k);
    [Qm, Qt] = get_rotation_matrices(x4(e));
    Anew = Qt * squeeze(Materials.Material1.Aglob(i,j,:,:)) * Qt';
    Knew = Qm * squeeze(Materials.Material1.Kglob(i,j,:,:)) * Qm';
    Bnew = Qm * squeeze(Materials.Material1.Bglob(i,j,:,:)) * Qm';
    vol_new = Materials.Material1.Pglob(i,j);
  else
    cellType(e) = 1;
    x1(e) = Materials.Material2.beta(min_beta_idx);
    Anew = squeeze(Materials.Material2.Aglob(min_beta_idx,:,:));
    Knew = squeeze(Materials.Material2.Kglob(min_beta_idx,:,:));
    Bnew = squeeze(Materials.Material2.Bglob(min_beta_idx,:,:));
    vol_new = Materials.Material2.Pglob(min_beta_idx);
  end
  
  Hnew.A(:,:,e) = Anew;
  Hnew.K(:,:,e) = Knew;
  Hnew.B(:,:,e) = Bnew;
  vol_vec(e) = vol_new;
end % elements


tvol = sum(vol_vec);

x(:,1) = x1;
x(:,2) = x2;
x(:,3) = x3;
x(:,4) = x4;


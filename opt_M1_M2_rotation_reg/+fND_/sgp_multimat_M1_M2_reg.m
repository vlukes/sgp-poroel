function [x,cellType,ppen] = sgp_multimat_debug(Geo,Materials,cellType,Algparam,itmax,x0,k0,lambda_phi,lambda_psi,sfepyFile)
% sequential global programming
%
%
% INPUT:
%   Geo:      geometry struct
%       - name:     name of the problem xml file and output files
%       - noe:      number of elements
%       - nx:       number of elements in x-direction (only in the non-cfs)
%       - ny:       number of elements in y-direction (only in the non-cfs)
%       - nz:       number of elements in z-direction (only in the non-cfs)
%       - sx:       length in x-direction (only in the non-cfs)
%       - sy:       length in y-direction (only in the non-cfs)
%       - sz:       length in z-direction (only in the non-cfs)
%       - dim:  dimension
%   Materials: contains 2 structures: Material1 and Material2
%   Algparam: algorithmical parameters
%             - penfl:    filter penalization factor (not used currently)
%             - rad:      filter radius
%             - vvol:     desired volume
%   itmax:    maximaliteration number
%   x0:       initial design
%   lambda_phi,lambda_psi:   J = lambda_phi * compliance + lambda_psi * permeability
%   sfepyFile: path to sfepy simulation file
% OUTPUT
%   x:        new design
%   celltype: flag list indicating which unit cell in which FE
%   ppen:     final penalty parameter for volume constraint

%% extract geometry
% number of elements
m = Geo.noe;

% define volume fraction
vvol = Algparam.vvol;

total_volume = m;

% in matlab case only regular volume possible
% THIS HAS TO BE ADAPTED FOR IRREGULAR MESH IN SFEPY
Geo.elemVol = ones(Geo.noe,1);

x = x0;

%% prepare filtering
if Algparam.filtering
  filt  = fND_.calcFilt(Geo,Algparam.rad);
  dAF  = (speye(m)-filt)'*(speye(m)-filt);
else
  filt = 0;
  dAF = [];
end

%% First evaluation of cost function, derivative, constraints, ...

% material interpolation
[H, vol_vec] = fND_.getMaterial_2sc(Geo,Materials,cellType,x);

% filter current design (using A tensor or parameters ...)
[R,Rf,ffilt,filt_vec_comp,filt_vec] = compute_filter_term(Geo,lambda_psi,Algparam,Materials.Material1,filt,cellType,x);

% evaluate cost function and derivative with respect to tensor H
tttf1=clock;
[J,DA,DK,DB,phi,psi] = call_sfepy_pe_extended(H.A,H.K,H.B,lambda_phi,lambda_psi,sfepyFile,0,x,vol_vec,R,Rf,filt_vec_comp,filt_vec);    
dfHm.A = DA;
dfHm.K = DK;
dfHm.B = DB;

tttf2=clock;
tttfd=etime(tttf2,tttf1);

%% initialize Lagrange multiplier for volume bisection
if Algparam.bisection
  ppen = 0.5*(Algparam.pmax+Algparam.pmin);
else
  ppen = 0;
end

fvol = sum(vol_vec);
%% calculate merit function
fmer = J + ppen * (fvol/total_volume-vvol)  + Algparam.penfl*ffilt;

%% initial parameters for optimization loop
nbisec = 0;   % bisection counter

L.A = zeros(6,6,m); % lower asymptote A
L.B = zeros(3,3,m); % lower asymptote B
L.K = zeros(3,3,m); % lower asymptote K

Algparam.L = L;
tau = Algparam.tau0;


% create .plot.dat file
fid = fopen(strcat(Geo.problem,'.txt'), 'a' );
        
fprintf(fid,'it    merit      vol      filt     f_obj      tau    ppen   time    timef   timesub  nsub  nprox phi psi\n');
fprintf(fid,'%4d %10.6e %f %8.2e %10.6e %10.6e %f %8.2e %8.2e %8.2e %3d %3d %10.6e %10.6e\n',...
        k0,fmer,fvol/total_volume, ffilt, J, tau, ppen, 0, 0, 0, nbisec, 0, phi, psi);
    
fprintf(1,'__________________________________________________________________________________________________________________________________\n');
fprintf(1,'it   |   merit\t    |   vol    |   filt   |   f_obj\t    |   tau \t    |   ppen   |   time   |   timef  | timesub  | nsub |  nprox  | phi  | psi\n');
fprintf(1,'______________________________________________________________________________________________________________________________________________________\n');
fprintf(1,'%4d | %10.6e | %f | %8.2e | %10.6e | %10.6e | %f | %8.2e | %8.2e | %8.2e | %3d | %3d | %10.6e | %10.6e \n',...
        k0,fmer,fvol/total_volume, ffilt, J, tau, ppen, tttfd, tttfd, 0, nbisec, 0, phi, psi);


% iteration counter
k = k0;

x_new = x;

%% outer iteration loop 
count_breaks = 0;
while k <= itmax
    
    fnew = inf; % new cost function value
    inner_it = 0; % inner iteration counter
    
    if Algparam.prox > 0
      tau = Algparam.tau0;
    end
    k = k + 1;

    nodescent = false;

    %% inner iteration loop (prox-point loop) 
    while fnew - fmer > 1e-6 % inner iteration: increase prox parameter until descent 
        if inner_it > Algparam.nmaxproxupdates
          nodescent = true;
          break;
        end
        ttt1=clock;
    
        tttsd=0;
        ttts1=clock;
        
        %% bisection for volume equality constraint
        [x_new,vol_vec,cellType,Hnew_sub,ppen,nbisec,tttsd] = volume_bisection(x,vol_vec,total_volume,cellType,dfHm,H,Materials,ppen,Algparam,tau,R,dAF,tttsd);
        
        ttts2=clock;
        tttsd=tttsd + etime(ttts2,ttts1); 

        % do nothing, available from sub problem!!
        H_new = Hnew_sub;
        
        tttf1=clock;

        % evaluate filter term for new design
        [R_new,Rf_new,ffilt,filt_vec_comp,filt_vec] = compute_filter_term(Geo,lambda_psi,Algparam,Materials.Material1,filt,cellType,x_new);
        
        %% update compliance c and derivative with respect to Y_new

        [J,DA,DK,DB,phi,psi] = call_sfepy_pe_extended(H_new.A,H_new.K,H_new.B,lambda_phi,lambda_psi,sfepyFile,k,x_new,vol_vec,R_new,Rf_new,filt_vec_comp,filt_vec);
        
        dfHmtmp.A = DA;
        dfHmtmp.K = DK;
        dfHmtmp.B = DB;
        tttf2=clock;
        tttfd=etime(tttf2,tttf1);
        
        
        %% update volume
        fvol = sum(vol_vec);
        vol = fvol/total_volume;

        %% update merit function
        fnew = J + ppen * (fvol-vvol*total_volume) + Algparam.penfl*ffilt;        

        if fnew-fmer > 1e-6
          fprintf(1,'tau = %10.6e \t fold = %10.6e \t fnew = %10.6e\n',tau,fmer,fnew);
          tau = tau * 10;
          % proximal update counter
          inner_it = inner_it+1;
        end
        if abs(fnew-fmer) < 1e-6
          count_breaks = count_breaks + 1;
          if tau > Algparam.tau0 % prox term is large, current point cannot be further improved
            nodescent = 1;
          end
          break;
        end
        
    end  % end of inner iteration
     
    
    %% update solution
    x = x_new;
    H = H_new;
    dfHm = dfHmtmp;
    fmer = fnew;

    %plotall_symm(x,Geo,.1);
%     plotall(x,Geo,Material,vol_vec,filt_vec);
             
    
    %% output and update iteration counter k
    ttt2=clock;
    % some output to screen ...
    fprintf(fid,'%4d %10.6e %f %8.2e %10.6e %10.6e %f %8.2e %8.2e %8.2e %3d %3d %10.6e %10.6e\n',...
        k,fmer,vol, ffilt, J, tau, ppen, etime(ttt2,ttt1), tttfd, tttsd, nbisec, inner_it, phi, psi);
    
    fprintf(1,'%4d | %10.6e | %f | %8.2e | %10.6e | %10.6e | %f | %8.2e | %8.2e | %8.2e | %3d | %3d | %10.6e | %10.6e \n',...
        k,fmer,vol, ffilt, J, tau, ppen, etime(ttt2,ttt1), tttfd, tttsd, nbisec, inner_it, phi, psi);
    
    % stop algorithm if objective hasn't changed in the last few iterations
    if count_breaks == 5 || nodescent
      break;
    end
end

% plotall(x,Geo,Material,vol_vec,filt_vec);
fprintf(1,'final design: fmer_last_min = %f\n',fmer);


fclose(fid);

end

function Jvar = plot_compliance(Jvar,angles,idx,x,H,lambda_psi,lambda_phi,sfepyFile,vol_vec)
  % index of element we want to vary
  cnt = 1;

  Aconst = [2.1417    0.7771    0.6568    0.0000    0.0000    0.0000
            0.7771    2.1417    0.6567    0.0000    0.0000    0.0000
            0.6568    0.6567    1.7919    0.0000    0.0000    0.0000
            0.0000    0.0000    0.0000    0.5904    0.0000    0.0000
            0.0000    0.0000    0.0000    0.0000    0.5904    0.0000
            0.0000    0.0000    0.0000    0.0000    0.0000    0.4866];
  
  Bconst = [ 0.0619    0.0000    0.0000
                  0    0.0619    0.0000
                  0         0    0.0669];
                
  Kconst = [9.6319   -0.0489   -0.0247
            -0.0489   9.6319    0.0282
          -0.0247     0.0282    2.6984];
  
  for a = angles
    % reset all tensors to intial design
    Aall = H.A;
    Ball = H.B;
    Kall = H.K;
    xall = x;
    
    [Qm, Qt] = get_rotation_matrices(a);
  
    % vary tensor of one element
    Arot = Qt*Aconst*Qt';
    Brot = Qm * Bconst * Qm';
    Krot = Qm * Kconst * Qm';
    % vary material tensors in one element
    Aall(:,:,idx) = Arot;
    Ball(:,:,idx) = Brot;
    Kall(:,:,idx) = Krot;
    xall(idx,4) = a;
    [J,DA,DK,DB,phi,psi] = call_sfepy_pe(Aall,Kall,Ball,lambda_phi,lambda_psi,sfepyFile,0,x,vol_vec);
    Jvar(cnt) = J;
    cnt = cnt + 1;
  end
  plot(angles,Jvar)
end

function [R,Rf,ffilt,filt_vec_comp,filt_vec] = compute_filter_term(Geo,lambda_psi,Algparam,Material1,filt,cellType,x)
  if Algparam.filtering
    R  = get_reg(Geo,Material1,cellType,x);
    Rf = filt*R;
    filt_vec_comp = R - Rf;
    filt_vec = sum((filt_vec_comp).^2, 2);
    ffilt = .5*sum(filt_vec); % compute squared distance to filtered design
  else
    R = [];
    Rf = [];
    ffilt = 0;
    filt_vec = [];
    filt_vec_comp = [];
  end
end

function [x_new,vol_vec_new,cellType_new,Hnew,ppen,ncounter,tttsd] = volume_bisection(x,vol_vec,total_volume,cellType,dfHm,H,Materials,ppen,Algparam,tau,R,dAF,tttsd)
  ncounter = 0;
  x_new = 0;
  Hnew = 0;
  cellType_new = cellType;
  vol_vec_new = vol_vec;
  if Algparam.bisection == 0
    ppen = 0;
  else % actual bisection loop
    pmax = Algparam.pmax;
    pmin = Algparam.pmin;
    vol = 999;
  end
  while ncounter < Algparam.nmaxbisections
    ttts1=clock;
    % re-solve sub problem to global optimality with updated multiplier for volume constraint!
    [x_new,~,~,vol_vec_new,Hnew_sub,cellType_new] = fND_.sub_solve_sgp_2sc_M1_M2_reg(x,cellType,dfHm,H,Materials,ppen,Algparam,tau,R,dAF);
    Hnew = Hnew_sub;
    ttts2=clock;
    tttsd=tttsd + etime(ttts2,ttts1);

    if Algparam.bisection == 0
      break;
    end

    %% calculate volume
    fvol = sum(vol_vec_new);
    vol = fvol/total_volume;

%     fprintf(1,'ppen = %10.6e \t vol = %10.6e\t diff = %10.6e\n',ppen,vol,abs(vol-Algparam.vvol));

    ncounter = ncounter + 1;
    
    if abs(vol-Algparam.vvol) < Algparam.bisection_tol
      break;
    end

    if vol > Algparam.vvol
      pmin = ppen;
      ppen  = .5*(pmax+ppen);
    else
      pmax = ppen;
      ppen  = .5*(pmin+ppen);
    end

  end
end  

function [R] = get_reg(Geo,Material1,cellType,x)
  nelem = Geo.noe;
  R = zeros(nelem,3); % choose 3d regularization label
  
  nsamples_alpha = Material1.nr_points;
  for e = 1:nelem
    if cellType(e) == 0
          [rows1, cols1] = find(Material1.alpha1 == x(e,2));
          [rows3, cols3] = find(Material1.alpha3 == x(e,3));
  
          % find indices where both row/column indices match
          row_idx = find(rows1 == rows3);
          col_idx = find(cols1 == cols3);
  
          % these are the indices to obtain respective material tensors for the initial values
          row_idx = rows1(row_idx);
          col_idx = cols1(col_idx);
          R(e,1) = (row_idx-1)/nsamples_alpha;
          R(e,2) = (col_idx-1)/nsamples_alpha;
          R(e,3) = cos(2*x(e,4)-pi/2.0);
    else % inclusion
      R(e,:)=-1;
    end
  end

end

function [filt] = calcFilt(Geo,radius)
% This function computes a 3D-Filter-Matrix
% 
% [filt] = calcFilt3(nx,ny,nz,radius)
%
% INPUT:
%   Geo:    geometry struct
%       - nx: number of elements in x-direction
%       - ny: number of elements in y-direction
%       - nz: number of elements in z-direction
%   radius: radius of filter
%
% OUTPUT
%   filt:   filter-matrix of size (nx*ny*nz,nx*ny*nz)
%

if radius == 0
    filt = speye(Geo.noe,Geo.noe);
    return;
end
%% prepare filter
iH  = ones(Geo.noe*(2*(ceil(radius)-1)+1)^2,1);
jH  = ones(size(iH));
sH  = zeros(size(iH));
k = 0;
nx = Geo.nx;
ny = Geo.ny;
nz = Geo.nz;

for i1 = 1:nx
  for k1 = 1:nz
    for j1 = 1:ny
      e1 = (i1-1)*ny*nz + (k1-1)*ny+j1;
      for i2 = max(i1-(ceil(radius)-1),1):min(i1+(ceil(radius)-1),nx)
        for k2 = max(k1-(ceil(radius)-1),1):min(k1+(ceil(radius)-1),nz)
          for j2 = max(j1-(ceil(radius)-1),1):min(j1+(ceil(radius)-1),ny)
            e2 = (i2-1)*ny*nz + (k2-1)*ny+j2;
            k = k+1;
            iH(k) = e1;
            jH(k) = e2;
            sH(k) = max(0,radius-sqrt((i1-i2)^2+(j1-j2)^2+(k1-k2)^2));
          end
        end
      end
    end
  end
end

%% write filter to sparse matrix
H    = sparse(iH,jH,sH);
tmp  = spdiags(1./sum(H,2),0,Geo.noe,Geo.noe);
filt = tmp*H; 


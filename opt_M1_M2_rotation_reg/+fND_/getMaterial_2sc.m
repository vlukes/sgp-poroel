function [H, vol_vec] = getMaterial_2sc(Geo,Materials,cellType,x)
% this function computes the material stiffness for given design
% 
% Y = getMaterial(Geo, Material, x, scale)
%
% INPUT:
%   Geo: geometry struct, eg. Geo.noe: number of finite elements
%   Material: material struct
%          - A,B,K:  material interpolation coefficients from material
%          - a,b,c:    discretization of each design variable in material catalog
%   cellType: flags indicating which unit cell type to use in which 
%   x:  design x(:,1) beta variable for inclusion cell type, x(:,2:4) thickness design parameter for fluid channels of other cell type
%   scale: optional scaling of material tensor, default: 1
%
% OUTPUT
%   H:        containing fields A, B and K
%
nelem = Geo.noe;
H.A = zeros(6,6,nelem);
H.K = zeros(3,3,nelem);
H.B = zeros(3,3,nelem);

Material1 = Materials.Material1;
Material2 = Materials.Material2;

% assume that initial design contains only 1st unit cell type
% assert(x(1,1) == 0);
% assume homogeneous initial design for alpha1 and alpha3
[rows1, cols1] = find(Material1.alpha1 == x(1,2));
% assert(size(rows1,1) > 1 && size(cols1,1) > 1);
[rows3, cols3] = find(Material1.alpha3 == x(1,3));
% assert(size(rows3,1) > 1 && size(cols3,1) > 1);

% find indices where both row/column indices match
row_idx = find(rows1 == rows3);
col_idx = find(cols1 == cols3);

% these are the indices to obtain respective material tensors for the initial values
row_idx = rows1(row_idx);
col_idx = cols1(col_idx);

% assume homogeneous design
Aconst = squeeze(Material1.Aglob(row_idx,col_idx,:,:));
Bconst = squeeze(Material1.Bglob(row_idx,col_idx,:,:));
Kconst = squeeze(Material1.Kglob(row_idx,col_idx,:,:));
vol_vec = zeros(nelem,1);

for e = 1:nelem
  if cellType(e) == 0
    % set initial design
    [Qm, Qt] = get_rotation_matrices(x(e,4));
    A = Qt * Aconst * Qt';
    B = Qm * Bconst * Qm';
    K = Qm * Kconst * Qm';
    vol_vec(e) = Material1.Pglob(row_idx,col_idx);
  else % inclusion
    idx = find( abs(Material2.beta - x(e,1)) < 1e-6);
    A = squeeze(Material2.Aglob(idx,:,:));
    B = squeeze(Material2.Bglob(idx,:,:));
    K = squeeze(Material2.Kglob(idx,:,:));
    vol_vec(e) = Material2.Pglob(idx);
  end
  
  H.A(:,:,e) = A;
  H.B(:,:,e) = B;
  H.K(:,:,e) = K;
end

end
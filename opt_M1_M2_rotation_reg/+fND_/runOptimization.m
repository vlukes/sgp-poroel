%% optimization
i0 = 0;

%% continuation with SIMP-type scheme
for i = 1:length(iterations)
    if i > 1
        i0 = sum(iterations(1:i-1));
    end
    imax = i0 + iterations(i);
    % main sgp algorithm
    [xopt{i+1},ppen] = fND_.sgp_multimat_M1_M2_reg(Geo,Materials,cellType,Algparam,imax,xopt{i},i0,lambda_phi,lambda_psi,sfepyFile);
end

save(strcat(Geo.problem,'.mat'), 'xopt', 'Geo')